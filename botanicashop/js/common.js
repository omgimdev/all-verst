$(document).ready(function() {
  
  //Burger menu
  $('.burger-wrap').click(function(){
    $(this).children().toggleClass('active');
    $(this).next().toggleClass('active');
    return false;
  });
  
  // Select2
  
  $('.select').select2();
  
  // Fancybox
  
  $('[data-fancybox]').fancybox({
	protect: true
  });
  
});





