$(document).ready(function() {
  
  //Burger
  
  $('.burger-wrap').click(function(){
    $(this).children().toggleClass('active');
    $('.menu').toggleClass('active');
    return false;
  });
  
  //Main-slider
  
  $('.main-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1
  });
  
  //Modal
  
  $('[data-fancybox]').fancybox({
	protect: true
  });
  
});





