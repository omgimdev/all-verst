$(document).ready(function() {
  
  // Burger
  
  $('.burger').on('click', function(){
    $(this).children().toggleClass('active');
    $('.header__nav').fadeToggle();
    return false;
  });

  var showcaseSlider = new Swiper('.js-showcase-slider.swiper-container', {
    navigation: {
      nextEl: '.showcase-button-next',
      prevEl: '.showcase-button-prev',
    },
    pagination: {
      el: '.showcase-pagination',
      type: 'bullets',
      clickable: true,
    },
  });







  $(window).on('resize', function() {
    if ($(window).width() > 991) {
      $('.header__nav').removeAttr('style');
    }
  });
});





