$(document).ready(function() {
  
  // Burger
  
  $(function(){
  
    $('.burger').click(function(){
      $(this).children().toggleClass('active');
      $('.nav-wrap').toggleClass('active');
      return false;
    });

    $(document).on('click', function(e) {
      if (!$(e.target).closest(".nav").length) {
        $('.nav-wrap').removeClass('active');
        $('.burger a').removeClass('active');
      }
      e.stopPropagation();
    });

  }());
  
  // Select2
  
  $('.options select').select2({widht: '100%'});
  $('.directory-sort select').select2();
  
  // Showcase
  
  $('.showcase-slider').slick({
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    prevArrow: '<button type="button" class="slick-prev"><span>предыдущий</span> <img src="images/right-arrow.png" alt=""></button>',
    nextArrow: '<button type="button" class="slick-next"><span>следующий</span> <img src="images/right-arrow.png" alt=""></button>'
  });
  
  // Review
  
  $('.review-items').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    variableWidth: true,
    prevArrow: '<button type="button" class="slick-prev"></button>',
    nextArrow: '<button type="button" class="slick-next"></button>',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });
  
  // Product
  
  $(function(){
    $('.product-slider').slick({
      appendArrows: $('.product-arrows'),
      speed: 200,
      fade: true,
      nextArrow: '<button type="button" class="slick-next"><img src="images/down.png" alt=""></button>',
      prevArrow: '<button type="button" class="slick-prev"><img src="images/up.png" alt=""></button>',
      responsive: [
      {
        breakpoint: 991,
        settings: {
          arrows: false
        }
      }
    ]
    });

    $('.product-dots li').click(function(){
      $('.product-dots li').removeClass('active');
      $(this).addClass('active');
      var index = $(this).index();
      $('.product-slider').slick('slickGoTo', index);
    });

    $('.product-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
      $('.product-dots li').removeClass('active');
      $('.product-dots li').eq(nextSlide).addClass('active');
    });
  });
  
});


