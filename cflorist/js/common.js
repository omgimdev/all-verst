$(document).ready(function () {
  
  //Burger menu
  $('.burger-wrap').click(function () {
    $(this).children().toggleClass('active');
    $('.header-menu-all').toggleClass('active');
    return false;
  });
  
  // Showcase hover
  $('.showcase-item__img').mouseenter(function () {
    $(this).next().addClass('active');
  });
  
  $('.showcase-item-hover').mouseleave(function () {
    $(this).removeClass('active');
  });
  
  // Slider
  
   $('.showcase-items-carousel').slick({
    slidesToShow: 4,
    slidesToScroll: 4,
    dots: true,
    responsive: [
    {
      breakpoint: 1340,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        dots: false
      }
    },
    {
      breakpoint: 450,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false
      }
    }
    ]
   });
  
  // Product radio
  
  $('.product-radio').click(function() {
    
    $('.product-input').removeClass('active');
    $(this).closest('.product-input').addClass('active');
    
  });
  
});