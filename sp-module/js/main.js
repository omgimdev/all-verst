$(document).ready(function() {
  
  // Burger
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.custom-nav').toggleClass('active');
    return false;
  });

  $(document).on('click', function(e) {
    if (!$(e.target).closest(".custom-nav").length) {
      $('.custom-nav').removeClass('active');
      $('.burger a').removeClass('active');
    }
    e.stopPropagation();
  });
  
  // Fancybox
  
  $('.modal-btn').click(function(){
    $.fancybox.close();
  });
  
  // Fullpage
  
  $('#fullpage').fullpage({
    resize: true,
    anchors : ['firstPage', 'secondPage'],
    scrollHorizontally: true,
    scrollingSpeed: 1000,
    fixedElements: '.header-wrap',
    paddingTop: '76px',
    responsiveWidth: 991
  });
  
});





