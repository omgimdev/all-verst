$(document).ready(function() {
  
  // Search block on mobile and tablet
  
  (function(){

     $('.search').click(function(e) {
      if ($(window).width() < 991 && $(window).width() > 767) {
        if ( !$(this).hasClass('active')) {
          e.preventDefault();
          $(this).addClass('active');
        }
      }
    });

    var over;

    $('.search').mouseenter(function() {
        over = true;
      })
      .mouseleave(function() {
        over = false;
      });

    $(document).click(function(e) {
      if (!over) {
        $('.search').removeClass('active');
      }
    });
   
  }());
  
  // Burger menu
  
  $('.burger-wrap').click(function(){
    $(this).children().toggleClass('active');
    $('.menu ul').toggleClass('active');
    return false;
  });
  
  
});





