$(document).ready(function() {
  
  $('.regions__current').click(function() {
    $('.regions-items').toggleClass('active');
  });
  
  $(document).click(function(e) {
    if(!e.target.closest('.regions')) {
      $('.regions-items').removeClass('active');
    }
  });
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.mobile-menu').toggleClass('active');
    return false;
  });
  
  // Shares
  
  $('.shares-items').slick({
    slidesToShow: 4,
    slidesToScroll: 2,
    variableWidth: true,
    dots: true,
    appendArrows: $('.arrows'),
    appendDots: $('.dots'),
    responsive: [
    {
      breakpoint: 1336,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 575,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1
      }
    }
  ]
  });
  
  // Manufacturers
  
  $('.manufacturers-items').slick({
    slidesToShow: 6,
    slidesToScroll: 3,
    variableWidth: true,
    dots: true,
    appendArrows: $('.custom-arrows'),
    appendDots: $('.custom-dots'),
    responsive: [
    {
      breakpoint: 1336,
      settings: {
        slidesToShow: 5
      }
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 4
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 575,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }
  ]
  });
  
  // Directory list
  
  $('.directory-head').click(function(){
    $(this).next().toggleClass('active');
  });
  
  // Product inform
  
  $('.product-inform-nav li').click(function(e){
    e.preventDefault();
    $('.product-inform-nav li').removeClass('active');
    $(this).addClass('active');
    $('.product-inform-item').hide().removeClass('active');
    $('.product-inform-item').eq($(this).index()).fadeIn().addClass('active');
  });
  
  // Goods slider
  
  $('.goods-items').slick({
    slidesToShow: 5,
    slidesToScroll: 2,
    variableWidth: true,
    dots: true,
    appendArrows: $('.goods-arrows'),
    appendDots: $('.goods-dots'),
    responsive: [
    {
      breakpoint: 1336,
      settings: {
        slidesToShow: 4
      }
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 575,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
  
  $('.goods-dots .slick-dots').hide();
  $('.goods-dots .slick-dots').eq(0).show();
  $('.goods-arrows .slick-prev').hide();
  $('.goods-arrows .slick-prev').eq(2).show();
  $('.goods-arrows .slick-next').hide();
  $('.goods-arrows .slick-next').eq(0).show();
  
  // Goods tabs
  
  $('.goods-nav li').click(function(e){
    e.preventDefault();
    $('.goods-nav li').removeClass('active');
    $(this).addClass('active');
    
    $('.goods-items').hide().removeClass('active');
    $('.goods-items').eq($(this).index()).fadeIn().addClass('active');
    
    $('.goods-dots .slick-dots').hide();
    $('.goods-dots .slick-dots').eq($(this).index()).show();
    $('.goods-arrows .slick-prev').hide();
    $('.goods-arrows .slick-next').hide();
    
    switch ($(this).index()) {
      case 0:
        $('.goods-arrows .slick-prev').eq(2).show();
        $('.goods-arrows .slick-next').eq(0).show();
        break;
      case 1:
        $('.goods-arrows .slick-prev').eq(1).show();
        $('.goods-arrows .slick-next').eq(1).show();
        break;
      case 2:
        $('.goods-arrows .slick-prev').eq(0).show();
        $('.goods-arrows .slick-next').eq(2).show();
        break;
    }
  });
  
  // Cabinet tabs
  
  $('.cabinet-nav li').click(function(){
    $('.cabinet-nav li').removeClass('active');
    $(this).addClass('active');
    $('.cabinet-item').hide().removeClass('active');
    $('.cabinet-item').eq($(this).index()).fadeIn().addClass('active');
  });
  
  // Goods slider
  
  $('.contacts-gallery').slick({
    slidesToShow: 1,
    slidesToScroll: 1
  });
  
  // Showcase carousel
  
  $('.showcase-carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true
  });
  
});





