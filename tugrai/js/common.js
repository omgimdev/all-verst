$(document).ready(function() {
  
  // Burger
  
  $(function(){
  
    $('.burger').click(function(){
      $(this).children().toggleClass('active');
      $('.nav').toggleClass('active');
      return false;
    });

    $(document).on('click', function(e) {
      if (!$(e.target).closest(".nav").length) {
        $('.nav').removeClass('active');
        $('.burger a').removeClass('active');
      }
      e.stopPropagation();
    });

  }());
  
  // Product
  
  $('.product-items').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    variableWidth: true,
    responsive: [
    {
      breakpoint: 1578,
      settings: {
        slidesToShow: 2
      }
    },
      {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
        arrows: false,
        centerMode: true
      }
    }
  ]
  });
  
  // Product inner
  
  $('.product-inner').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 2,
    variableWidth: true,
    dots: true,
    responsive: [
    {
      breakpoint: 1578,
      settings: {
        slidesToShow: 2
      }
    },
      {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
        arrows: false,
        centerMode: true
      }
    }
  ]
  });
  
  // Main services, main event
  
  $('.main-services-items, .main-event-items').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    variableWidth: true,
    responsive: [
    {
      breakpoint: 1578,
      settings: {
        slidesToShow: 3
      }
    },
      {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2
      }
    },
      {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
        arrows: false,
        centerMode: true
      }
    }
  ]
  });
  
  // Slider
  
  $('.slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 2,
    variableWidth: true,
    dots: true,
    responsive: [
    {
      breakpoint: 1578,
      settings: {
        slidesToShow: 3
      }
    },
      {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2
      }
    },
      {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
        arrows: false,
        centerMode: true
      }
    }
  ]
  });
  
  // Catalog select
  
  $('.catalog-nav__list').select2();
  
  // Fancybox
  
  if( $('[data-fancybox]').length) {
    $('[data-fancybox]').fancybox();
  }
  
  // Header info modal
  
  $(function(){
    
    $('.header-info__region').click(function(){
      if( $(this).hasClass('active') ) {
        $(this).removeClass('active');
        $('.modal').addClass('active');
      }
      else {
        $(this).addClass('active');
        $('.modal').removeClass('active');
      }
    });
    
    $('.modal-close').click(function(){
      $('.header-info__region').addClass('active');
      $('.modal').removeClass('active');
    });
    
    $(document).on('click', function(e) {
      if (!$(e.target).closest(".header-info-label").length) {
        $('.modal').removeClass('active');
        $('.header-info__region').addClass('active');
      }
      e.stopPropagation();
    });
    
  });
  
  
  
});





