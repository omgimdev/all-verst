$(document).ready(function() {
  
  // Burger
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.menu-wrap').toggleClass('active');
    return false;
  })

  // Clients carousel

  $('.clients-items').slick({
    slidesToShow: 5,
    slidesToSlide: 1,
    prevArrow: '<button class="slick-arrow slick-prev"></button>',
    nextArrow: '<button class="slick-arrow slick-next"></button>',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  })

  // Article gallery

  $('.article-gallery').slick({
    dots: true,
    prevArrow: '<button class="slick-arrow slick-prev"></button>',
    nextArrow: '<button class="slick-arrow slick-next"></button>',
  })

  // Reviews carousel

  $('.reviews-items').slick({
    arrows: false,
    fade: true
  });

  $('.reviews-arrows .prev').click(function() {
    $('.reviews-items').slick('slickPrev');
  });

  $('.reviews-arrows .next').click(function() {
    $('.reviews-items').slick('slickNext');
  });

});





