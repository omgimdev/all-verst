$(document).ready(function() {
  
  //Burger menu
  $('.burger-wrap a').click(function(){
    $(this).toggleClass('active');
    $('.nav').toggleClass('active')
    return false;
  });
  
  //About
  
  $('.about-item-head').click(function(){
    
    var content = $(this).next();
    var parent = $(this).parent();
    
    if(parent.hasClass('active') ) {
      content.slideUp();
      parent.removeClass('active');
    }
    else {
      $('.about-item-content').slideUp();
      $('.about-item').removeClass('active');
      content.slideDown();
      parent.addClass('active')
    }
    
  });
  
  
});





