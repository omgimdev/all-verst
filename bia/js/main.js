$(document).ready(function() {
  
  // Burger
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.nav').toggleClass('active');
    return false;
  })

  // Partners

  $('.partners-items').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    appendArrows: '.partners-arrows',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
        }
      },
    ]
  })
  
});





