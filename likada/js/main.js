$(document).ready(function() {
  
  // Burger
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.main-nav').toggleClass('active');
    $('.header-section').toggleClass('active');
    return false;
  });
  
  // Showcase
  
  $('.showcase').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    appendArrows: '.showcase-arrows',
    appendDots: '.showcase-dots'
  });
  
  // Partners
  
  $('.partners-tabs li').click(function(){
    $('.partners-tabs li').removeClass('active');
    $(this).addClass('active');
    $('.partners-item').hide().removeClass('active');
    $('.partners-item').eq($(this).index()).fadeIn().addClass('active');
  });
  
  // Showcase
  
  $('.details-items').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
  });
  
  
});





