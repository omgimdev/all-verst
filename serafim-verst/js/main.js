$(document).ready(function () {

  $('.showcase__carousel').slick();

  $('.places__carousel').slick({
    centerMode: true,
    centerPadding: 80
  });

  var tempArray = [];

  $('.places__carousel').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    $('.places__item').removeClass('filter');

    $('.places__item').each(function () {
      if (!$(this).hasClass('slick-cloned')) {
        tempArray.push($(this))
      }
    })

    tempArray[nextSlide].addClass('filter');
  });

  $('.master-class__carousel').slick({
    centerMode: true,
    centerPadding: 80
  });

  $('.master-class__carousel').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    $('.master-class__item').removeClass('filter');

    $('.master-class__item').each(function () {
      if (!$(this).hasClass('slick-cloned')) {
        tempArray.push($(this))
      }
    })

    tempArray[nextSlide].addClass('filter');
  });

  $('.faq__head').on('click', function () {
    if ($(this).parent().hasClass('active')) {
      $('.faq__item').removeClass('active');
      $('.faq__body').slideUp();
    } else {
      $('.faq__item').removeClass('active');
      $('.faq__body').slideUp();

      $(this).parent().addClass('active');
      $(this).next().slideDown();
    }
  });

});





