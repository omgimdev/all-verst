$(document).ready(function () {

  // Timer

  $('.start-timer').countdown({
    until: new Date(2019, 6, 20),
    format: 'HMS',
    layout: '<div class="start-timer-body">' +
    '<div class="value">{hnn}</div>' +
    '<span>:</span>' +
    '<div class="value">{mnn}</div>' +
    '<span>:</span>' +
    '<div class="value">{snn}</div>'
    +'</div>',
  });


});