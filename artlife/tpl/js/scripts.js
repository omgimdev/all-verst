$(function() {

	$('.header .menu-toggle').click(function(e) {
		e.preventDefault();
		var $menu = $('.header .menu'),
			menuHeight = 0,
			windowHeight = 0;

		if ($('body').hasClass('menu-opened')) {
			$('body').removeClass('menu-opened');
			$menu.css('margin-top', 0);
		} else {
			$('body').addClass('menu-opened');
			menuHeight = $menu.height();
			windowHeight = $(window).height();
			$menu.css('margin-top', (windowHeight - menuHeight) / 2 - 20);
		}
	});

	$('.top-slider').each(function() {
		$(this).owlCarousel({
			singleItem: true,
			navigation: true,
			pagination: false,
			slideSpeed: 500
		});
	});

	$('.teachers-item .related .items').each(function() {
		$(this).owlCarousel({
			items: 3,
			navigation: true,
			pagination: false,
		});
	});
	
	$('.course-item .short-descr').each(function() {
		var $big = $(this).find('.big .pic'),
			$thumbs = $(this).find('.thumbs'),
			$sidebar = $thumbs.parent(),
			itemHeight = $thumbs.find('li:first').height(),
			items = $thumbs.find('li').size();

		$(this).find('.thumbs a').click(function(e) {
			e.preventDefault();
			$(this).parent().addClass('active').siblings().removeClass('active');
			$big.css('background-image', 'url("' + $(this).attr('href') + '")');
		}).first().click();

		if (items > 3) {
			$(this).append('<span class="pr"></span><span class="nx"></span>');
			
			$(this).find('.pr').click(function() {
				$thumbs.animate({
					scrollTop: '-=' + itemHeight
				}, 400, toggleMasks);
			});

			$(this).find('.nx').click(function() {
				$thumbs.animate({
					scrollTop: '+=' + itemHeight
				}, 400, toggleMasks);
			});
		}
		
		function toggleMasks() {
			if ($thumbs.scrollTop() === 0) {
				$sidebar.addClass('top');
			} else {
				$sidebar.removeClass('top');
			}

			if ($thumbs.scrollTop() == $thumbs.prop('scrollHeight') - $thumbs.height()) {
				$sidebar.addClass('bottom');
			} else {
				$sidebar.removeClass('bottom');
			}
		}
	});

	$('.students').each(function() {
		var $thumbs = $(this).find('.thumbs'),
			itemHeight = $thumbs.find('li:first').height();

		$(this).append('<span class="pr"></span><span class="nx"></span>');
		
		$(this).find('.pr').click(function() {
			$thumbs.animate({
				scrollTop: '-=' + itemHeight
			}, 400);
		});

		$(this).find('.nx').click(function() {
			$thumbs.animate({
				scrollTop: '+=' + itemHeight
			}, 400);
		});
	});

	$('.mythes').each(function() {
		var $items = $(this).find('.items'),
			$first = $items.find('.item:first'),
			itemWidth = $first.outerWidth(true),
			wrapMargin = ($(window).width() - $('.wrap').first().width()) / 2;
		
		$items.children('.inner')
			.width(itemWidth * $items.find('.item').size() - parseInt($first.css('margin-right'), 10))
			.css('padding-left', wrapMargin)
			.css('padding-right', wrapMargin);
		
		$(this).find('.pr').click(function() {
			$items.animate({
				scrollLeft: '-=' + itemWidth
			}, 400);
		});

		$(this).find('.nx').click(function() {
			$items.animate({
				scrollLeft: '+=' + itemWidth
			}, 400);
		});
	});

	

	
	$('.content .certs-list a').attr('rel', 'cert').fancybox();

	$('.footer .reviews').each(function(i) {
		var $reviews = $(this),
			$slider = $(this).find('.items');

		$reviews.find('select').selectmenu();
		$reviews.find('.add-review a').click(function(e) {
			e.preventDefault();
			$reviews.addClass('form-opened');
		});

		$reviews.find('.reviews-form .btn a').click(function(e) {
			e.preventDefault();
			$reviews.removeClass('form-opened');
		});

		$reviews.find('form').submit(function(e) {
			e.preventDefault();
			var $form = $(this),
				$submit = $form.find('input[type="submit"]');

			$.post('/ajax/review.php', $form.serialize(), function(data) {
				showTooltip($submit, data);
			});
		});

		$slider.owlCarousel({
			singleItem: true,
			navigation: false,
			pagination: true
		});
	});

	$('input[type="file"]').change(function() {
		if ($(this).val().length > 0) {
			$(this).siblings('.name').html('Файл<br>прикреплен');
		}
	});

	function showTooltip($el, title) {
		$el.tooltip({
			title: title,
			html: true,
			trigger: 'malual'
		}).tooltip('show');

		setTimeout(function() {
			$el.tooltip('hide').tooltip('destroy');
		}, 5000);
	}

});