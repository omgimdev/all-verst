$(document).ready(function () {

  //Burger

  $('.burger').click(function () {
    $(this).children().toggleClass('active');
    $('.header__nav').toggleClass('active');
    return false;
  });

  // Slick

  $('.success__slider-items').slick({
    infinite: false,
  });

  $('.all').text('0' + $('.success__slider-item').length)

  $('.success__slider-items').on('afterChange', function(event, slick, currentSlide){
    $('.current').text('0' + (currentSlide+1))
  });

  // Dropdown

  $('.connection__field-input--region input').click(function () {
    $('.connection__search').toggleClass('active');
  })


});





