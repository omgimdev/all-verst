$(document).ready(function() {

  $('.language').on('click', '.language__item', function() {
    $('.language__item').removeClass('language__item--active');
    $(this).addClass('language__item--active');
  });

  $('.burger').on('click', function(){
    $(this).children().toggleClass('active');
    $('.header__content').fadeToggle().toggleClass('header__content--show');
    return false;
  });

  var showcaseSlider = new Swiper('.js-showcase-slider.swiper-container', {
    fadeEffect: { crossFade: true },
    virtualTranslate: true,
    // autoplay: {
    //     delay: 2000,
    //     disableOnInteraction: true,
    // },
    speed: 600, 
    slidersPerView: 1,
    effect: 'fade',
    navigation: {
      nextEl: '.showcase-button-next',
      prevEl: '.showcase-button-prev',
    },
    pagination: {
      el: '.showcase-pagination',
      type: 'bullets',
      clickable: true,
    },
  });

  var companiesSlider = new Swiper('.js-companies-slider.swiper-container', {
    slidesPerView: 1,
    navigation: {
      nextEl: '.companies-button-next',
      prevEl: '.companies-button-prev',
    },
    breakpoints: {
      575: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      767: {
        slidesPerView: 3,
        spaceBetween: 26
      },
      991: {
        slidesPerView: 4,
        spaceBetween: 26
      }
    },
  });

  $('.basket__amount').on('click', function(event) {
    var $target = $(event.target);
    var $input = $(this).find('.basket__amount-input > input');
    var value  = parseInt($input.val().replace(/\s+/g, ""));

    if ($target.hasClass('basket__amount-minus') && value > 1) {
      $input.val(value - 1);
    }
    if ($target.hasClass('basket__amount-plus')) {
      $input.val(value + 1);
    }
  });




  $(window).on('resize', function() {
    if ($(window).width() > 767 && !$('.header__content').hasClass('header__content--show')) {
      $('.header__content').removeAttr('style');
    }
  });
});





