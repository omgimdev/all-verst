$(document).ready(function() {
  
  $(function(){
  
    $('.burger').click(function(){
      $(this).children().toggleClass('active');
      $('.nav').toggleClass('active');
      return false;
    });

    $(document).on('click', function(e) {
      if (!$(e.target).closest(".nav").length) {
        $('.nav').removeClass('active');
        $('.burger a').removeClass('active');
      }
      e.stopPropagation();
    });

  }());
  
  // Scroll to advantage block

  $(function(){

    $(".showcase-scroll").click(function() {

      $("html, body").animate({
        scrollTop: $($(this).attr("href")).offset().top + "px"
      }, {
        duration: 1000
      });

      return false;
    });

  }());
  
  // Compare mobile slider
  
  $('.compare-items').slick({
    infinite: false,
    appendArrows: $('.arrows')
  });
  
  $('.all').text($('.compare-item').length);
  
  $('.compare-items').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    $('.current').text(nextSlide + 1);
  });
  
  // Issues item
  
  $('.issues-item-head').click(function(){
    
    var content = $(this).next();
    var parent = $(this).parent();
    
    if( parent.hasClass('active') ) {
      content.slideUp();
      parent.removeClass('active');
    } 
    else {
      $('.issues-item-content').slideUp();
      $('.issues-item').removeClass('active');
      content.slideDown();
      parent.addClass('active');
    }
  });

  // Product carousel
  
  $(function(){
    $('.product-carousel').slick({
      speed: 200,
      fade: true,
      responsive: [
      {
        breakpoint: 991,
        settings: {
          arrows: false
        }
      }
    ]
    });

    $('.product-dots li').click(function(){
      $('.product-dots li').removeClass('active');
      $(this).addClass('active');
      var index = $(this).index();
      $('.product-carousel').slick('slickGoTo', index);
    });
  });
  
});





