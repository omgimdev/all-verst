$(document).ready(function() {
  
  // Burger
  
  function burger(btn, elem) {
    $(btn).click(function(){
      $(this).children().toggleClass('active');
      $(elem).toggleClass('active');
      return false;
    })
  }

  burger('.top-burger', '.top-nav');
  burger('.nav-burger', '.nav');

  // Product change amount

  var current, input;
  $('.amount span').click(function() {
    input = $(this).closest('.amount').find('input');
    current = +input.val();
    if( $(this).hasClass('plus') ) {
      current++;
    } else {
      if( current !== 1 ) {
        current--;
      }
    }
    input.val(current);
  })

  // Product gallery

  $('.product-gallery-main').slick({
    speed: 200,
    fade: true,
    prevArrow: '<button type="button" class="slick-prev">&lt;</button>', 
    nextArrow: '<button type="button" class="slick-next">&gt;</button>',
  })

  $('.product-gallery-thumbs li').click(function(){
    $('.product-gallery-thumbs li').removeClass('active');
    $(this).addClass('active');
    $('.product-gallery-main').slick('slickGoTo', $(this).index());
  })

  $('.product-gallery-main').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    $('.product-gallery-thumbs li').removeClass('active');
    $('.product-gallery-thumbs li').eq(nextSlide).addClass('active');
  })

  // Article

  $('.article-carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    prevArrow: '<button type="button" class="slick-prev">&lt;</button>', 
    nextArrow: '<button type="button" class="slick-next">&gt;</button>',
  })

})