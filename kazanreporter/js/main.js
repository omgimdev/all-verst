$(document).ready(function() {
  
  // Burger
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.nav-wrap').toggleClass('active');
    return false;
  });
  
  // Search
  
  $('.search-btn').click(function(){
    $(".search input").addClass("active").focus;
    $(this).addClass("active");
    return false;
  });
  
  $(document).click(function(e) {
    if (!$(e.target).closest(".search").length) {
      $(".search input").removeClass("active").focus;
      $(this).removeClass("active");
    }
    e.stopPropagation();
  });
  
  // Nav burger
  
  $('.burger-nav').click(function() {
    $(this).toggleClass('active');
    $(this).siblings('.nav-sub').toggleClass('active');
    return false;
  });
  
  // Important
  
  $('.important-items').slick({
    slidesToShow: 4,
    slidesToScroll: 4,
    arrows: false,
    variableWidth: true,
    dots: true,
    responsive: [
    {
      breakpoint: 1336,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    },
      {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
      {
      breakpoint: 575,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
  ]
  });
  
});





