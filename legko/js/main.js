$(document).ready(function() {
  
  // Choose current city
  
  $('.header-city__current').click(function() {
    $(this).toggleClass('active');
    $('.header-city-items').toggleClass('active');
  });
  
  $(document).click(function(e) {
    if(!e.target.closest('.header-city')) {
      $('.header-city-items').removeClass('active');
      $('.header-city__current').removeClass('active');
    }
  });
  
  // Main slider
  
  $('.main-slider').slick({
    prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
    dots: true
  });
  
  // Programms tabs
  
  $('.programms__link').click(function(e) {
    e.preventDefault();
    if(!$(this).hasClass('active')) {
      $('.programms__link').removeClass('active');
      $(this).addClass('active');
      $('.programms-tab').hide();
      $($(this).attr('href')).show();
    }
  });
  
  // Courses slider
  
  $('.courses-carousel').slick({
    prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
    dots: true,
    slidesToShow: 2,
    slidesToScroll: 2,
    responsive: [
      {
        breakpoint: 1336,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
  
  // Reviews carousel
  
  $('.reviews-carousel').slick({
    prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>'
  });
  
  // Courses slider
  
  $('.teachers-carousel').slick({
    prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
    dots: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
    ]
  });
  
  // Burger
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.header-nav ul').toggleClass('active');
    return false;
  });
  
  $('.burger__title').click(function(){
    $('.burger a').toggleClass('active');
    $('.header-nav ul').toggleClass('active');
  });
  
  // Issues
  
  $('.issues-head').click(function(){
    if(!$(this).hasClass('active')){
      $('.issues-head').removeClass('active');
      $(this).addClass('active');
      $('.issues-content').slideUp();
      $(this).next().slideDown();
    }else{
      $(this).removeClass('active');
      $(this).next().slideUp();
    }
  });
  
  // Our teachers slider
  
  $('.our-teachers-items').slick({
    prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
    slidesToShow: 1,
    slidesToScroll: 1
  });
  
  // Article carousel
  
  $('.article-items').slick({
    dots: true,
    prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
    slidesToShow: 1,
    slidesToScroll: 1
  });
  
  // Advantages
  
  $('.advantages-items').slick({
    dots: true,
    prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
    slidesToShow: 2,
    slidesToScroll: 2,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 1336,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
  
});





