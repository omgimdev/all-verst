$(document).ready(function() {
  
  // Burger
  
  $('.burger-wrap').click(function(){
    $(this).children().toggleClass('active');
    $('.nav ul').toggleClass('active');
    return false;
  });
  
  // Showcase
  
  $('.showcase-slider-wrap').slick({
    dots: true,
    arrows: false,
    infinite: true,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
  
  // Result
  
  $('.result-slider-wrap').slick({
    arrows: true,
    infinite: true,
    responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
  
   // Slider
  
  $('.slider').slick({
    arrows: true,
    infinite: true,
    variableWidth: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1360,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
      {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
      {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
      {
      breakpoint: 450,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
  
  // Issues
  
  $('.issues-item-head').click(function(){
    
    var parent = $(this).parent();
    var content = $(this).next();
    
    if( parent.hasClass('active') ) {
      content.slideUp();
      parent.removeClass('active');
    } 
    else {
      $('.issues-item-content').slideUp();
      $('.issues-item').removeClass('active');
      content.slideDown();
      parent.addClass('active');
    }
    
  });
  
  // Fancybox
  
  if( $('[data-fancybox]').length) {
    $('[data-fancybox]').fancybox();
  }
  
  
  
});





