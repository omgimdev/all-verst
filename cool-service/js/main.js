$(document).ready(function() {
  
  // Burger
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.nav').toggleClass('active');
    return false;
  })

  // Customers

  $('.customers-items').slick({
    slidesToShow: 3,
    slidesToScroll: 2,
    dots: true,
    responsive: [
      {
        breakpoint: 1336,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
        }
      },
    ]
  })

  // We offer

  $('.we-offer-items').slick({
    appendArrows: $('.we-offer-arrows'),
    speed: 200,
    fade: true
  });

  $('.we-offer-items').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
    $('.we-offer-nav .current').text(nextSlide + 1);
  });

  $('.we-offer-nav .all').text($('.we-offer-item').length);

  // Work performed

  $('.work-performed-items').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
  })

  // Content gallery

  $('.content-slides').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    appendDots: $('.content-dots'),
    appendArrows: $('.content-arrows'),
  })

});





