$(document).ready(function() {
  
  // Burger
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.header-block').toggleClass('active');
    return false;
  })
  
  // City list

  $('.city-head a').click(function(e) {
    e.preventDefault();
    $(this).closest('.city').children('.city-select').fadeToggle();
  })

  // Input animate

  function check_input(input) {
    $(input).blur(function(){
      if ($(this).val() != '') {
        $(this).addClass('complete');
      } else {
        $(this).removeClass('complete');
      }
    })
  }

  check_input('.order-form-item input');
  check_input('.request-item input');

  // Input mask

  if($(".order-form-tel").length) {
    $(".order-form-tel").inputmask({
      mask: "+7 ( 9 9 9 ) ' 9 9 9 ' 9 9 ' 9 9",
      showMaskOnHover: false,
      alternatormarker: "|"
    })
  }

  if($(".request-tel").length) {
    $(".request-tel").inputmask({
      mask: "+7 ( 9 9 9 ) ' 9 9 9 ' 9 9 ' 9 9",
      showMaskOnHover: false,
      alternatormarker: "|",
    })
  }

  // Check form
  
  $('.check-wrap').click(function () {
    if ($(this).find('.check-input').is(':checked')) {
      $('.order-form .link').removeAttr('disabled');
    } else {
      $('.order-form .link').attr('disabled', true);
    }
  })

  // Range slider

  $('.calc-slider').each(function() {
    var that = $(this);
    that.ionRangeSlider({
      min: 220,
      max: 400,
      from: 310,
      hide_min_max: true,
      hide_from_to: true,
      onStart: function (data) {
        that.prev().find('.irs-handle').append('<img src="../images/calc-arrow.png" alt="">' + '<div class="number">' + data.from + ' руб./лид</div>');
      },
      onChange: function(data) {
        that.prev().find('.number').html(data.from + ' руб./лид');

        if(data.from_percent < 10) {
          that.prev().find('.number').addClass('left');
        } else {
          that.prev().find('.number').removeClass('left');
        }

        if(data.from_percent > 90) {
          that.prev().find('.number').addClass('right');
        } else {
          that.prev().find('.number').removeClass('right');
        }
      }
    })
  })

  var number_clients, conversion;

  $('.calc-slider').change(function() {
    number_clients = +$('#number-clients').data('from');
    conversion = +$('#conversion').data('from');

    $('.franchise-invest__info span').text(number_clients + conversion);
  })

  // Order form fixed

  var form,
      parent,
      pos_left;

  parent = $('.with-fixed').parent().css({
    position: 'relative'
  })
  
  $(window).scroll(function() {
    $('.with-fixed').each(function() {
      form = $(this);
      parent = form.parent();
      pos_left = parent.offset().left;
      var pos_top = parent.offset().top - 10;
      if ($(window).scrollTop() > pos_top) {
        form.addClass('fixed').css({
          left: pos_left
        });
      } else {
        form.removeClass('fixed').css({
          left: '0px'
        })
      }
      if ($(window).scrollTop() > pos_top + parent.outerHeight() - form.outerHeight()) {
        form.addClass('stopped')
      } else {
        form.removeClass('stopped')
      }
    })
  });

  $(window).resize(function() {
    $('.with-fixed').each(function() {
      form = $(this);
      parent = form.parent();
      pos_left = parent.offset().left;
      if (form.hasClass('fixed')) {
        form.css({
          left: pos_left
        });
      }
    });
  })

  // Provide, Product about, Dimensions

  function tabs(tab, item) {
    $(tab).click(function(){
      $(item).hide().removeClass('active');
      $(item).eq($(this).index()).fadeIn().addClass('active');
      $(tab).removeClass('active');
      $(this).addClass('active');
    })
  }

  tabs('.provide-nav li', '.provide-item');
  tabs('.product-about-nav li', '.product-about-item');
  tabs('.dimensions-nav li', '.dimensions-item');

  // Projects

  $('.projects-items').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
        }
      },
    ]
  })

  // Product gallery
  
  $('.product-gallery-mini').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    vertical: true,
    responsive: [
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 2,
          vertical: false,
          variableWidth: true,
        }
      },
    ]
  })

  $('.product-gallery-mini li').click(function() {
    $('.product-gallery-main img').attr('src', $(this).find('img').attr('src'));
    $('.product-gallery-mini li').removeClass('active');
    $(this).addClass('active');
  })

  // Projects

  $('.product-category-items').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 5,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 4,
        }
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 2,
        }
      },
    ]
  })

  // Object carousel
  
  $('.object-carousel-mini').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 2,
        }
      },
    ]
  })

  $('.object-carousel-mini li').click(function() {
    $('.object-carousel-main img').attr('src', $(this).find('img').attr('src'));
    $('.object-carousel-mini li').removeClass('active');
    $(this).addClass('active');
  })

  // Process

  $('.process-slides').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    appendArrows: '.process-arrows',
  })
  
  // About group

  $('.about-group-items').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    appendArrows: '.about-group-arrows',
  })

  // About employee

  $('.about-employee-items').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    appendArrows: '.about-employee-arrows',
  })

  // Projects over

  $('.projects-over-items').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    appendArrows: '.projects-over-arrows',
  })

  // Thanks

  $('.thanks-items').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    variableWidth: true,
    appendArrows: '.thanks-arrows',
    responsive: [
      {
        breakpoint: 1336,
        settings: {
          slidesToShow: 5,
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 4,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 2,
        }
      },
    ]
  })

  // Foundation suitable

  $('.foundation-suitable-items').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 5,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 4,
        }
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 2,
        }
      },
    ]
  })

  // FAQ

  $('.faq-head').click(function(){
    var content = $(this).next();
    var parent = $(this).parent();
    if(parent.hasClass('active')) {
      content.slideUp();
      parent.removeClass('active');
      $(this).removeClass('active');
    } else {
      $('.faq-content').slideUp();
      $('.faq-item').removeClass('active');
      $('.faq-head').removeClass('active');
      $(this).addClass('active');
      content.slideDown();
      parent.addClass('active');
    }
  })

})