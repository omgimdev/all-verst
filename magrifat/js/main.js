$( document ).ready(function() {

	$('.owl-carousel').owlCarousel({
      loop:true,
      margin:50,
      responsiveClass:true,
      dots:true,
      responsive:{
          0:{
              items:1,
              nav:true
          },
          480:{
              items:2,
              nav:true
          },
          600:{
              items:3,
              nav:true,
              margin:30
          },
          1000:{
              items:4,
              nav:true,
              loop:true
          }
      }
  });
    $('.carousel-inner').owlCarousel({
        loop:true,
        margin:50,
        responsiveClass:true,
        dots:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            480:{
                items:2,
                nav:true
            },
            600:{
                items:3,
                nav:true,
                margin:30
            },
            1000:{
                items:4,
                nav:true,
                loop:true
            }
        }
    });




  $(window).scroll(function() {
    if ($(this).scrollTop() > 110){  
      $('.header').addClass("sticky");
      // $('.unregistered').slideDown("slow");
    }
    else{
      $('.header').removeClass("sticky");
      // $('.unregistered').slideUp("slow");
    }
  });

});//end ready