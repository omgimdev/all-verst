$(document).ready(function() {
  
  // Main partners
  $('.partners-items').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    appendArrows: '.partners-arrows',
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
      }
    },
      {
      breakpoint: 767,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
      }
    },
      {
      breakpoint: 450,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
      }
    }
  ]
  });
  
  // Slider
  $('.slider-items').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    appendArrows: '.slider-about-arrows'
  });
  
  // Products slider
  $('.products-slider-items').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    appendArrows: '.products-slider-arrows',
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
      }
    },
      {
      breakpoint: 451,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
      }
    }
  ]
  });
  
  //Burger
  $('.burger-wrap').click(function(){
    $(this).children().toggleClass('active');
    $('.nav').toggleClass('active');
    return false;
  });
  
  $('[data-fancybox]').fancybox({
	protect: true
  });
  
});
