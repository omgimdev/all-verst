$(document).ready(function() {
  
  // Burger
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.custom-nav').toggleClass('active');
    return false;
  });

  $(document).on('click', function(e) {
    if (!$(e.target).closest('.custom-nav').length){
      $('.custom-nav').removeClass('active');
      $('.burger a').removeClass('active');
    }
    if(!$(e.target).closest('.story').length){
      $('.story-body').removeClass('active');
      $('.story .link').removeClass('active');
    }
    e.stopPropagation();
  });
  
  // Catalog map
  
  $('.catalog-map-link').click(function(e){
    e.preventDefault();
    $('.catalog-map').slideToggle();
    
    if(!($(this).hasClass('active'))) {
      $(this).addClass('active');
    }else {
      $(this).removeClass('active');
    }
  });
  
  // Gallery change image

  $('.product-gallery-mini li a').click(function(e) {
    e.preventDefault();
    var img_src = $(this).find('img').attr('src');
    $('.product-gallery-main img').attr('src', img_src);
  });
  
  // Modal input
  
  $('.modal-input').on( 'blur', function() {
    if ($(this).val() !== "") {
      $(this).parent().addClass('empty');
    } else {
      $(this).parent().removeClass('empty');
    }
  });
  
  // Catalog story
  
  $('.story .link').click(function(e){
    e.preventDefault();
    $(this).toggleClass('active');
    $(this).next().toggleClass('active');
  });
  
});





