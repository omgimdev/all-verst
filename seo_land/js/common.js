$(document).ready(function() {
  
  // Fullpage
  $('.page').fullpage({
    verticalCentered: false,
    responsiveWidth: 992,
    afterLoad: function() {
      slideGraph()
    },
    anchors: ['slide1', 'slide2', 'slide3', 'slide4', 'slide5', 'slide6', 'slide7', 'slide8', 'slide9', 'slide10'],
    menu: '.menu'
  });
  
  //Carousel  
  $('.c-slider-items').slick({
    speed: 300
  });
  
  $('#client-slider').slick('unslick');
  
  $('#client-slider').slick({
    asNavFor: '.clients-nav'
  });
  
  $('.clients-nav').on('afterChange', function(event, slick, currentSlide, nextSlide){
    createCurrent();
  });
  
  $('.c-slider-nav__item').click(function() {
    
    var index = $(this).index();
    var parent = $(this).closest('.c-slider');
    var slider = parent.find('.c-slider-items');
    
    slider.slick('slickGoTo', index);
    parent.find('.c-slider-nav__item').removeClass('active');
    $(this).addClass('active');
    
  });
  
  $('.c-slider-items').on('afterChange', function(event, slick, currentSlide, nextSlide){
    var parent = $(this).closest('.c-slider');
    var nav = parent.find('.c-slider-nav__item');
    nav.removeClass('active');
    nav.eq(currentSlide).addClass('active');
  });
  
  $('.clients-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '#client-slider',
    autoplay: true,
    autoplaySpeed: 5000
  });
  
  function createCurrent() {
    $('.clients-nav-item').removeClass('active');
    $('.clients-nav-item.slick-current').next().next().addClass('active');
  }
  
  createCurrent();
  
  $(window).resize(function() {
    clientUnslick();
    faq_deactivate();
  });
  
  function clientUnslick() {
    if($(window).width() < 992) {
      $('.clients-nav').slick('destroy');
      $('.clients-nav-item').each(function() {
        if( $(this).index() > 4 ) {
          $(this).addClass('mob-hidden');
        }
      });
    }
    else {
      $('.clients-nav').slick('init');
    }
  }
  
  clientUnslick();
  
  // Burger menu
  
  $('.burger-wrap').click(function(){
    $(this).children().toggleClass('active');
    $('.nav ul').toggleClass('active');
    return false;
  });
  
  $('.menu li a').click(function() {
    $('.burger-wrap a').removeClass('active');
    $('.nav ul').removeClass('active');
  });
  
  // Fancybox
  
  $('.fancybox').fancybox();
  
  // Advan graph
  
  var count = 0;
  
  $(window).scroll(function() {
    
    slideGraphScroll();
    
  });
  
  function itearte(time, el) {
    setTimeout(function(){
      el.addClass('active');
    },time*300)
  }
  
  function slideGraph() {
    if( $('.advan-wrap').hasClass('active') ) {
      $('.advan-graph__item').each(function() {
        var el = $(this);
        itearte(count, el);
        count++;
      });
    }
  }
  
  function slideGraphScroll() {
    if( $('.advan-wrap').offset().top <= $(window).scrollTop() ) {
      $('.advan-graph__item').each(function() {
        var el = $(this);
        itearte(count, el);
        count++;
      });
    }
  }
  
  // Faq 
  
  $('.faq-item__head').click(function(){
    
    if( $(this).hasClass('active') ) {
      $(this).next().slideUp();
      $(this).removeClass('active');
    }
    else {
      $(this).parent().parent().find('.faq-item__answer').slideUp();
      $(this).parent().parent().find('.faq-item__head').removeClass('active');
      $(this).next().slideDown();
      $(this).addClass('active');
    }
  });
  
  function faq_deactivate() {
    if( $(window).width() < 992 ) {
      $('.faq-col').last().children().first().removeClass('active');
      $('.faq-col').last().children().first().children().first().removeClass('active');
    }
  }
  
  faq_deactivate();
  
});





