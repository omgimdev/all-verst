$(document).ready(function() {
  
  // Burger
  
  function burger(btn, item) {
    $(btn).click(function(){
      $(this).children().toggleClass('active');
      $(item).toggleClass('active');
      return false;
    });
  }
  
  burger('.top-burger', '.top-nav');
  burger('.nav-burger', '.nav');
  burger('.record-burger', '.header-menu');
  burger('.headline-burger', '.headline-nav');
  
  
  // Showcase
  
  $('.showcase-items').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
  });
  
  // Team
  
  $('.team-items').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    variableWidth: true
  });
  
  // Charity
  
  $('.charity-items').slick({
//    speed: 200,
    fade: true,
    appendDots: '.charity-thumbs',
  });

  $('.charity-thumbs li').click(function(){
    $('.charity-thumbs li').removeClass('active');
    $(this).addClass('active');
    $('.charity-items').slick('slickGoTo', $(this).index());
  });

  $('.charity-items').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    $('.charity-thumbs li').removeClass('active');
    $('.charity-thumbs li').eq(nextSlide).addClass('active');
  });
  
  // Faq
  
  function dropDownList(current, content) {
    $(current).click(function(){
      if( $(this).hasClass('active') ) {
        $(this).next().slideUp();
        $(this).removeClass('active');
      } else {
        $(content).slideUp();
        $(current).removeClass('active');
        $(this).addClass('active');
        $(this).next().slideDown();
      }
    });
  }
  
  dropDownList('.faq-item-header', '.faq-elems');
  dropDownList('.doctor-item-header', '.doctor-item-body');
  
  // Select2
  
  if($('.select2').length) {
    $('.select2').select2({
      width: '100%',
      minimumResultsForSearch: Infinity
    });
    $('.select2.with-search').select2({
      width: '100%'
    });
  }
  
  // Specialists
  
  $('.specialists-items').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    variableWidth: true,
    appendArrows: $('.team-arrows'),
    appendDots: $('.team-dots'),
  });
  
  // Team tabs
  
  $('.team-nav li').click(function(e){
    e.preventDefault();
    $('.team-body').hide().removeClass('active');
    $('.team-body').eq($(this).index()).fadeIn().addClass('active');
    $('.team-nav li').removeClass('active');
    $(this).addClass('active');
  });

  // Main tabs arrows

  $('.team-arrows .slick-arrow').click(function() {
    var slider = $(this).closest('.team-body').find('.team-items');
    $(this).hasClass('slick-prev') ? slider.slick('slickPrev') : slider.slick('slickNext')
  });

  // Datepicker
  
  if($('.datepicker-calendar').length) {
    $.fn.datepicker.dates['ru'] = {
      days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
      daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб"],
      daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
      months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
      monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
      today: "Сегодня",
      weekStart: 1,
      monthsTitle: 'Месяцы',
      format: 'yyyy-mm-dd',
    };
    $('.datepicker-calendar').datepicker({
      language: 'ru',
      datesDisabled: ['2018-12-05', '2018-12-06', '2018-12-07', '2018-1-22', '2018-12-24', '2018-12-25'],
      startDate: new Date(),
      templates: {
        leftArrow: '<i class="fas fa-angle-left"></i>',
        rightArrow: '<i class="fas fa-angle-right"></i>'
      }
    });
  }

  if($('.check-in-datepicker').length) {
    $.fn.datepicker.dates['ru'] = {
      days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
      daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб"],
      daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
      months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
      monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
      today: "Сегодня",
      weekStart: 1,
      monthsTitle: 'Месяцы',
      format: 'dd-mm-yyyy',
    };
    $('.check-in-datepicker').datepicker({
      language: 'ru',
      templates: {
        leftArrow: '<i class="fas fa-angle-left"></i>',
        rightArrow: '<i class="fas fa-angle-right"></i>'
      },
      orientation: "bottom left",
    });
  }

  $('.datepicker-switch').click(function(e){
    e.stopPropagation();
  });

  $('.datepicker-time-item.disabled').click(function(e){
    e.preventDefault();
  });

  // Doctor info list

  $('.doctor-info__link').click(function(e) {
    e.preventDefault();
    $(this).css('display', 'none');
    $('.doctor-info').css('height', 100 + '%');
  });

  // Assign

  $('.assign-policy .checkbox input').change(function() {
    if($(this).prop('checked')) {
      $('.assign-policy-item').fadeIn().css('display', 'flex');
    } else {
      $('.assign-policy-item').fadeOut();
    }
  });

  // Data fancybox

  if($('[data-fancybox]').length) {
    $('[data-fancybox]').fancybox({
      autoFocus : false,
      btnTpl : {
        smallBtn: '<button data-fancybox-close class="fancybox-close-small"><span>Закрыть</span><img src="../images/close.png" alt=""></button>'
      },
    });
  }

  // Headline search

  $('.m-search-btn').click(function(){
    $(".m-search input").toggleClass("active").focus;
    $(this).toggleClass("active");
    return false;
  });

  // Scroll headline

  $(window).scroll(function() {
    if($(this).scrollTop() > 300) {
      $('.headline').addClass('show');
    } else {
      $('.headline').removeClass('show');
    }

    if($('.headline-nav').hasClass('active')) {
      $('.headline .burger a').removeClass('active');
      $('.headline-nav').removeClass('active');
    }
  });
  
});





