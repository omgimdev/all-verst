$(document).ready(function() {
  
  //Top line
  function header_pop() {
    $(window).scroll(function() {
      if($(window).width() > 991) {
        if($(window).scrollTop() > 159) {
          $('.header-wrap').addClass('header-mini');
          $('body').css({"padding-top" : "159px"});
          $('.header-mini').stop().animate({"top": "0"},200)
        }
        else {
          $('.header-mini').stop().animate({"top": "-75px"});
          $('.header-wrap').removeClass('header-mini');
          $('body').css({"padding-top" : "0"});
        }
      }
      else {
        $('body').css({"padding-top" : "0"});
        $('.header-wrap').removeClass('header-mini');
      }
    });
  }
  
  $(window).resize(function() {
    if($(window).width() < 991) {
      $('body').css({"padding-top" : "0"});
      $('.header-wrap').removeClass('header-mini');
    }
  })
  
  header_pop();
  
  
  
  // carousel
  
  $('.carousel').slick({
    arrows: false,
    draggable: false,
    touchMove: false
  });
  
  // carousel sport fees
  
  
  
  if($('.carousel').hasClass('carousel_sport-fees')) {
    
    $('.carousel-nav--all_sport-fees').html($('.carousel_sport-fees .carousels-item').size() - 2);
    
    $('.carousel--prev_sport-fees').click(function() {
      $('.carousel_sport-fees').slick('slickPrev');
      var current = $('.carousel_sport-fees').slick('slickCurrentSlide');
      $('.carousel-nav--current_sport-fees').html(current + 1);
    });

    $('.carousel--next_sport-fees').click(function() {
      $('.carousel_sport-fees').slick('slickNext');
      var current = $('.carousel_sport-fees').slick('slickCurrentSlide');
      $('.carousel-nav--current_sport-fees').html(current + 1);
    });
    
    $('.carousel-nav--prev_sport-fees').click(function() {
      $('.carousel_sport-fees').slick('slickPrev');
      var current = $('.carousel_sport-fees').slick('slickCurrentSlide');
      $('.carousel-nav--current_sport-fees').html(current + 1);
    });
    
    $('.carousel-nav--next_sport-fees').click(function() {
      $('.carousel_sport-fees').slick('slickNext');
      var current = $('.carousel_sport-fees').slick('slickCurrentSlide');
      $('.carousel-nav--current_sport-fees').html(current + 1);
    });

    
  }
  
  //carousel sport complex
  
  if($('.carousel').hasClass('carousel_sport-complex')) {
    
    $('.carousel-nav--all_sport-complex').html($('.carousel_sport-complex .carousels-item').size() - 2);
    
    $('.carousel--prev_sport-complex').click(function() {
      $('.carousel_sport-complex').slick('slickPrev');
      var current = $('.carousel_sport-complex').slick('slickCurrentSlide');
      $('.carousel-nav--current_sport-complex').html(current + 1);
    });

    $('.carousel--next_sport-complex').click(function() {
      $('.carousel_sport-complex').slick('slickNext');
      var current = $('.carousel_sport-complex').slick('slickCurrentSlide');
      $('.carousel-nav--current_sport-complex').html(current + 1);
    });
    
    $('.carousel-nav--prev_sport-complex').click(function() {
      $('.carousel_sport-complex').slick('slickPrev');
      var current = $('.carousel_sport-complex').slick('slickCurrentSlide');
      $('.carousel-nav--current_sport-complex').html(current + 1);
    });
    
    $('.carousel-nav--next_sport-complex').click(function() {
      $('.carousel_sport-complex').slick('slickNext');
      var current = $('.carousel_sport-complex').slick('slickCurrentSlide');
      $('.carousel-nav--current_sport-complex').html(current + 1);
    });

  }
  
  $('.trainers-carousel').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows : true,
    responsive : 
    [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 527,
        settings: {
          slidesToShow: 1
        }
      }
      
    ]
  })
  
  //Drop menu
    
  function drop_menu() {
    $('.header-drop').on('click', function(){
      var elem = $('.header-nav');
      if(elem.hasClass('nav-ul_fade')) {
        elem.hide().removeClass('nav-ul_fade');
      }
      else elem.fadeIn().addClass('nav-ul_fade');
    });

    
    var over;

    $('.header-drop, .header-nav').mouseenter(function() {
      over = true;
    })
    .mouseleave(function() {
      over = false;
    });

    $(document).click(function(e) {
      if (!over) {
        if($(window).width() < 768) {
          $('.header-nav').removeClass('nav-ul_fade').hide();
        }
      }
    });
  }
  
  drop_menu();

  
  //fancybox
  
  if($('.fancybox').hasClass('fancybox')) {
    $('.fancybox').fancybox({
      padding : 0
    });
  }
  
  
  
});