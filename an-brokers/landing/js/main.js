$(document).ready(function () {

    $(function () {

        $('.burger').click(function () {
            $(this).children().toggleClass('active');
            $('.nav').toggleClass('active');
            return false;
        });

        $(document).on('click', function (e) {
            if (!$(e.target).closest(".nav").length) {
                $('.nav').removeClass('active');
                $('.burger a').removeClass('active');
            }
            e.stopPropagation();
        });

    }());

    // City select

    $('.city > a').click(function (e) {
        e.preventDefault();
        $(this).next().fadeToggle();
    });

    // Calc range

    var calc_cost = 1500000,
        calc_start = 1000000;

    $(".calc-value").slider({
        range: "min",
        value: calc_cost,
        min: 1,
        max: 10000000,
        slide: function (event, ui) {
            $(".calc-value-amount").val(ui.value);
            calc_cost = +ui.value;
            calc1();
        },
        change: function (event, ui) {
            $(".calc-contribution").slider("option", "max", ui.value);
        }
    });

    $(".calc-value-amount").val($(".calc-value").slider("value"));

    $(".calc-value-amount").change(function () {
        var value = $('.calc-value-amount').val();
        console.log(value);
        $(".calc-value").slider("value", value);
    });


    $(".calc-contribution").slider({
        range: "min",
        value: calc_start,
        min: 1,
        max: calc_cost,
        step: 1,
        slide: function (event, ui) {
            if (ui.value > calc_cost) return false;
            $(".calc-contribution-amount").val(ui.value);
            calc_start = +ui.value;
            calc1();
        }
    });
    $(".calc-contribution-amount").val($(".calc-contribution").slider("value"));

    $(".calc-contribution-amount").change(function () {
        var value = $('.calc-contribution-amount').val();
        console.log(value);
        $(".calc-contribution").slider("value", value);
    });


    $(".calc-rate").slider({
        range: "min",
        value: 8,
        min: 0,
        max: 20,
        slide: function (event, ui) {
            $(".calc-rate-amount").val(ui.value + '%');
        }
    });

    $(".calc-rate-amount").val($(".calc-rate").slider("value") + ' %');

    $(".calc-rate-amount").change(function () {
        var value = $('.calc-rate-amount').val();
        console.log(value);
        $(".calc-rate").slider("value", value);
    });


    $(".calc-term").slider({
        range: "min",
        value: 15,
        min: 1,
        max: 30,
        slide: function (event, ui) {
            $(".calc-term-amount").val(ui.value);
        }
    });

    $(".calc-term-amount").val($(".calc-term").slider("value"));

    $(".calc-term-amount").change(function () {
        var value = $('.calc-term-amount').val();
        console.log(value);
        $(".calc-term").slider("value", value);
    });

    // Issues

    $('.issues-head').click(function () {
        var content = $(this).next();

        if (!($(this).hasClass('active'))) {
            $('.issues-head').removeClass('active');
            $('.issues-content').slideUp();
            $(this).addClass('active');
            content.slideDown();
        } else {
            $(this).removeClass('active');
            content.slideUp();
        }
    });

    function calc1() {
        $('.credit-sum').html((calc_cost - calc_start) + ' P');
    }

});





