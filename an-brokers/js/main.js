$(document).ready(function() {
  
  // Burger
  
  $(function(){
  
    $('.burger').click(function(){
      $(this).children().toggleClass('active');
      $('.navigation').toggleClass('active');
      return false;
    });

    $(document).on('click', function(e) {
      if (!$(e.target).closest(".navigation").length) {
        $('.navigation').removeClass('active');
        $('.burger a').removeClass('active');
      }
      e.stopPropagation();
    });

  }());
  
  // City select
  
  $('.city > a').click(function(e){
    e.preventDefault();
    $(this).next().fadeToggle();
  });
  
  // Object
  
  $('.object-items').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    variableWidth: true,
    responsive: [
    {
      breakpoint: 1201,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1
      }
    }
  ]
  });
  
  // Calc range
  
  $( ".calc-value" ).slider({
    range: "min",
    value: 1500000,
    min: 1,
    max: 10000000,
    slide: function( event, ui ) {
      $( ".calc-value-amount" ).val( ui.value );
    }
  });
  
  $( ".calc-value-amount" ).val( $( ".calc-value" ).slider( "value" ) );
  
  $(".calc-value-amount").change(function(){
    var value = $('.calc-value-amount').val();
    $(".calc-value").slider("value", value);
  });
  
  
  $( ".calc-contribution" ).slider({
    range: "min",
    value: 2000000,
    min: 1,
    max: 10000000,
    slide: function( event, ui ) {
      $( ".calc-contribution-amount" ).val( ui.value );
    }
  });
  $( ".calc-contribution-amount" ).val( $( ".calc-contribution" ).slider( "value" ) );
  
  $(".calc-contribution-amount").change(function(){
    var value = $('.calc-contribution-amount').val();
    $(".calc-contribution").slider("value", value);
  });
  
  
  $( ".calc-rate" ).slider({
    range: "min",
    value: 8,
    min: 0,
    max: 20,
    slide: function( event, ui ) {
      $( ".calc-rate-amount" ).val( ui.value + '%' );
    }
  });
  
  $( ".calc-rate-amount" ).val( $( ".calc-rate" ).slider( "value" ) + ' %' );
  
  $(".calc-rate-amount").change(function(){
    var value = $('.calc-rate-amount').val();
    console.log(value);
    $(".calc-rate").slider("value", value);
  });
  
  
  $( ".calc-term" ).slider({
    range: "min",
    value: 15,
    min: 1,
    max: 30,
    slide: function( event, ui ) {
      $( ".calc-term-amount" ).val( ui.value );
    }
  });
  
  $( ".calc-term-amount" ).val( $( ".calc-term" ).slider( "value" ) );
  
  $(".calc-term-amount").change(function(){
    var value = $('.calc-term-amount').val();
    $(".calc-term").slider("value", value);
  });
  
  // About slider
  
  $('.about-slider, .objects-gallery').slick({
    slidesToShow: 1,
    slidesToScroll: 1
  });
  
  // Gallery change image

  $('.product-gallery-mini li a').click(function(e) {
    e.preventDefault();
    var img_src = $(this).find('img').attr('src');
    $('.product-gallery-main img').attr('src', img_src);
    if(!($(this).hasClass('active'))) {
      $('.product-gallery-mini li a').removeClass('active');
      $(this).addClass('active');
    }
  });
  
  // Obj gallery mini
  
  $('.product-gallery-mini').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    vertical: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 575,
        settings: {
          vertical: false,
          slidesToShow: 3
        }
      }
    ]
  });
  
  // Calc slider
  
  $('.calc-slider').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    variableWidth: true
  });
  
  // Product article
  
  $('.showcase-services-navigation li').click(function(e){
    e.preventDefault();
    var current = $(this).index();
    
    $('.showcase-services-navigation li').removeClass('active');
    $(this).addClass('active');
    
    $('.product-article').hide().removeClass('active');
    $('.product-article').eq(current).fadeIn().addClass('active');
  });

  // Compare

  $('.compare-tabs li').click(function(e){
      e.preventDefault();
      $('.compare-item').hide().removeClass('active');
      $('.compare-info').hide().removeClass('active');
      $('.compare-item').eq($(this).index()).fadeIn().addClass('active');
      $('.compare-info').eq($(this).index()).fadeIn().addClass('active');
      $('.compare-tabs li').removeClass('active');
      $(this).addClass('active');
  });

  // Compare slider

  $('.compare-slider').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.compare-info-items',
      swipe: false,
      variableWidth: true,
  });

  $('.compare-info-items').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      asNavFor: '.compare-slider',
      swipe: false,
      variableWidth: true,
  });
});





