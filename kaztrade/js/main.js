$(document).ready(function() {
  
  // Burger
  
  $('.burger').click(function(){
    $(this).toggleClass('active');
    $(this).children().toggleClass('active');
    $('.nav-wrap').toggleClass('active');
    $('.logo').toggleClass('active');
    return false;
  })

  // Nav scroll

  $(window).scroll(function(){
    if($('.nav-wrap').hasClass('active')) {
      $('.nav-wrap').removeClass('active');
      $('.logo').removeClass('active');
      $('.burger').removeClass('active');
      $('.burger a').removeClass('active');
    }
  })

  // Search

  $('.search-btn').click(function(){
    $(".search input").toggleClass("active").focus;
    $(this).toggleClass("active");
    return false;
  })

  // Lang

  $('.lang-header').click(function() {
    $(this).parent().toggleClass('active');
    $(this).next().toggleClass('active');
  })

  // Nav

  $('.nav-list > a').click(function(e) {
    e.preventDefault();
    if($(this).hasClass('active')) {
      $(this).removeClass('active');
      $(this).next().slideUp();
    } else {
      $('.nav-sub').slideUp();
      $(this).next().slideDown();
      $('.nav-list > a').removeClass('active');
      $(this).addClass('active');
    }
  })

  // Showcase carousel

  $('.showcase-carousel').each(function() {
    $(this).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      appendArrows: $(this).parent().find('.slider-arrows'),
      dots: true,
    })
  })

  // About carousel

  $('.about-carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
  })

  // Guidebook

  $('.guidebook-items').each(function() {
    $(this).slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      appendArrows: $(this).parent().find('.slider-arrows'),
      variableWidth: true,
      responsive: [
        {
          breakpoint: 1336,
          settings: {
            slidesToScroll: 1,
          }
        }
      ]
    })
  })

  // Tourism

  $('.tourism-items').each(function() {
    $(this).slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      appendArrows: $(this).parent().find('.slider-arrows'),
      variableWidth: true,
      responsive: [
        {
          breakpoint: 1336,
          settings: {
            slidesToShow: 2,
          }
        }
      ]
    })
  })
  
  // Achieve

  $('.achieve-carousel').each(function() {
    $(this).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      variableWidth: true,
    })
  })

  // Datepicker
  
  if($('.datepicker-calendar').length) {
    $.fn.datepicker.dates['ru'] = {
      days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
      daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб"],
      daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
      months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
      monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
      today: "Сегодня",
      weekStart: 1,
      monthsTitle: 'Месяцы',
      format: 'dd-mm-yyyy',
    };
    $('.datepicker-calendar').datepicker({
      language: 'ru',
      startDate: new Date(),
      templates: {
        leftArrow: '<i class="fas fa-angle-left"></i>',
        rightArrow: '<i class="fas fa-angle-right"></i>'
      }
    }).datepicker('update', new Date());
  }

  // Select2

  if($('.select2').length) {
    $('.select2').select2({
      width: '100%',
    });
  }

  // Project tabs

  $('.project-links li').click(function(e) {
    e.preventDefault();
    $('.project-links li').removeClass('active');
    $(this).addClass('active');
    $('.project-item').hide().removeClass('active');
    $('.project-item').eq($(this).index()).fadeIn().addClass('active');
  })

  
  if(window.Scrollbar) {
    window.Scrollbar.init(document.querySelector('#scroll'));
  }
  
});





