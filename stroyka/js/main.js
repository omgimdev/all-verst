$(document).ready(function() {
  
  $(function(){
  
    $('.burger').click(function(){
      $(this).children().toggleClass('active');
      $('.nav-wrap').toggleClass('active');
      return false;
    });

    $(document).on('click', function(e) {
      if (!$(e.target).closest(".nav-wrap").length) {
        $('.nav-wrap').removeClass('active');
        $('.burger a').removeClass('active');
      }
      e.stopPropagation();
    });

  }());
  
  
  
});





