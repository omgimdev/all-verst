$(document).ready(function() {
  
  // Burger
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.header__nav ul').toggleClass('active');
    return false;
  })

  $('.showcase__carousel').slick({
    dots: true
  })

  $('.projects-page__carousel').slick();

  $('.services__items').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    swipeToSlide: true,
    dots: true,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
        }
      },
    ]
  })

  var handleProjectsCarousel = function() {
    var settings = {
      infinite: false,
      slidesToShow: 2,
      slidesToScroll: 1,
      swipeToSlide: true,
      arrows: true,
      appendArrows: $('.projects__nav'),
      responsive: [
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 1,
            appendArrows: null,
          }
        },
      ]
    };
    var onClickArrow = function() {
      $('.projects__nav .slick-prev').on('click', function() {
        $(slider).slick('slickPrev');
      });
      $('.projects__nav .slick-next').on('click', function() {
        $(slider).slick('slickNext');
      });
    };

    var slider = $('.projects__carousel').slick(settings);
    onClickArrow();

    $(window).on('resize', function() {
      var arrows = slider.find('.projects__nav');
      var arrowLength = arrows.first().find('.slick-arrow').length;
      
      if (arrowLength > 2) {
        slider.slick('unslick');
        arrows.empty();
        slider.slick(settings);
        onClickArrow();
      }
    });
  };
  handleProjectsCarousel();
});




