$(document).ready(function() {
  
  // Burger
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.header-content').toggleClass('active');
    return false;
  });
  
  // Smooth scrolling
  
  $(".showcase-scroll").click(function(){ 
    $("html, body").animate({
      scrollTop: $($(this).attr("href")).offset().top + "px"
    }, {
      duration: 1000
    });
    return false;
  });
  
  // Scrollbar
  
  var Scrollbar = window.Scrollbar;

  Scrollbar.init(document.querySelector('.product-info'), {
    alwaysShowTracks: true
  });
  
  Scrollbar.init(document.querySelector('.product-scroll'), {
    alwaysShowTracks: true
  });
  
});





