$(document).ready(function () {

    // fancybox
    $('.fancybox').fancybox({
        padding: 0
    });


     $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 50,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            480: {
                items: 2,
                nav: true
            },
            600: {
                items: 3,
                nav: true
            },
            1000: {
                items: 5,
                nav: true,
                loop: true
            }
        }
    })

});//end ready