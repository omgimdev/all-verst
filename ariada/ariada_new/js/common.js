$(document).ready(function() {
  
  //Burger menu
  
  $('.burger-wrap a').click(function(){
    $(this).toggleClass('active');
    $('.nav--ul').toggleClass('nav--ul_active')
    return false;
  });
  
  // Showcase
  
  $('.showcase-wrap').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    fade: true
  });
  
  // Shopfront
  
  $('.shopfront-items').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right"></i></button>',
    responsive: [
      {
        breakpoint: 1690,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1023,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
  
  // Feature
  
  $('.feature-items').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    responsive: [
    {
      breakpoint: 1336,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
      {
      breakpoint: 769,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
  
  // Feature
  
  $('.partners-items').slick({
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    responsive: [
    {
      breakpoint: 1700,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 1
      }
    },
      {
      breakpoint: 1336,
      settings: {
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 2
      }
    },
      {
      breakpoint: 769,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }
  ]
  });
  
   // Callback
  
  $('.callback-bond-nav__item').click(function(){
    
    var current = $(this).index();
    
    $('.callback-bond-content').hide();
    $('.callback-bond-content').eq(current).fadeIn();
    $('.callback-bond-nav__item').removeClass('active');
    $(this).addClass('active');
  });
  
  // Products slider
  
  $('.products-slider-items').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    variableWidth: true,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
      {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
  
  // Certificate
  
  if( $('[data-fancybox]').length) {
    $('[data-fancybox]').fancybox();
  }
  
  // Search hidden input
  
  $('.header-search button').click(function(e) {
    
    if ( !$(this).parent().hasClass('active')) {
      e.preventDefault();
      $(this).parent().addClass('active');
    }
    
  });
  
  var over;

  $('.header-search').mouseenter(function() {
      over = true;
    })
    .mouseleave(function() {
      over = false;
    });

  $(document).click(function(e) {
    if (!over) {
      $('.header-search').removeClass('active');
    }
  });
  
  // Product nav
  
  $('.product-class-nav li').click(function(){
    
    var current = $(this).index();
    
    $('.product-class-items').hide();
    $('.product-class-items').eq(current).fadeIn();
    $('.product-class-nav li').removeClass('active');
    $(this).addClass('active');
    
  });
  
  // Cabinet nav
  
  $('.cabinet-nav li').click(function(){
    
    var current = $(this).index();
    
    $('.cabinet-items').hide();
    $('.cabinet-items').eq(current).fadeIn();
    $('.cabinet-nav li').removeClass('active');
    $(this).addClass('active');
    
  });
  
  //register button disable
  
  $('.register__check-in').click(function(e) {
    if( !$('.register-info-check input')[0].checked ) {
      e.preventDefault();
    }
  });
  
});





