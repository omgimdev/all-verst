$(document).ready(function() {
  
  //Burger menu
  $('.burger-wrap').click(function(){
    $(this).children().toggleClass('active');
    $(this).next().toggleClass('active');
    return false;
  });
  
  //Carousels
  $('.main-carousel').slick({
    speed: 300
  });
  
  $('.bonus-carousel').slick({
    slidesToShow: 2,
    slidesToScroll: 2,
    dots: true,
    speed: 300,
    responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
  
  $('.card-view-carousel').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    speed: 300
  });
  
  $('.another-carousel').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    speed: 300,
    responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
  
  // Call tabs
  $('.call-sw-item').click(function() {
    
    $('.call-sw-item').removeClass('active');
    $(this).addClass('active');
    
    $('.call-item').removeClass('active');
    $('.call-item').eq($(this).index()).addClass('active');
    
  });
  
  //Drop cart
  
  $('.goods').click(function() {
    
    if( !$('.overlay').hasClass('active') ) {
      $('.overlay').fadeIn().addClass('active');
      $('.drop-cart-wrap').addClass('active');
    }
    else {
      $('.overlay').fadeOut().removeClass('active');
      $('.drop-cart-wrap').removeClass('active');
    }
    
  });
  
  $('.overlay').click(function() {
    
    $('.overlay').fadeOut().removeClass('active');
    $('.drop-cart-wrap').removeClass('active');
    
  });
  
  //Fancybox
  if( $('.fancybox').length ) {
    $('.fancybox').fancybox({
      padding: 0
    });
  }
  
  //Slider 
  if( $( ".sidebar-slider" ).length ) {
    
    $( ".sidebar-slider" ).slider({
      range: true,
      min: 0,
      max: 50000,
      values: [ 2500, 21500 ],
      slide: function( event, ui ) {
        $( "#input1" ).val( ui.values[ 0 ] );
        $( "#input2" ).val( ui.values[ 1 ] );
      }
    });
    
      $( ".sidebar-catalog-input input" ).change(function() {
      $( ".sidebar-slider" ).slider( "option", "values", [
        $( "#input1" ).val(),
        $( "#input2" ).val()
      ] );
    });
    
  }
  
  // Catalog item toggle
  $('.sidebar-catalog--head').click(function() {
    
    $(this).next().slideToggle(300);
    $(this).parent().toggleClass('active-arrow');
    
  });
  
  // Stick footer
  
  function footer_stick() {
    $('body').css("padding-bottom" , $('.footer-wrap').outerHeight());
  }
  
  footer_stick();
  
  $(window).resize(function() {
    footer_stick();
  });
  
  
});





