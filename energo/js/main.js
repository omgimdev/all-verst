$(document).ready(function () {

  // Burger

  $('.burger').click(function () {
    $(this).children().toggleClass('active');
    $('.header-nav').toggleClass('active');
    return false;
  });

  // Scroll down



  $('.showcase-down').on('click', function (e) {
    $("html, body").animate({
      scrollTop: $($(this).attr("href")).offset().top
    }, {
      duration: 1000
    });

    return false;
  });

  $('.projects-items').slick();

  $('.docs-items').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 450,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  $('.gallery').slick();

});





