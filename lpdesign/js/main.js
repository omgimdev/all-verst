$(document).ready(function () {

  // Label animate

  $('.callback-input input').blur(function () {
    if ($(this).val() != '') {
      $(this).addClass('complete');
    } else {
      $(this).removeClass('complete');
    }
  });


  // Input mask

  if ($(".callback-tel").length) {
    $('.callback-tel').inputmask({
      mask: "+7 4\\95 999-99-99",
      showMaskOnHover: false,
    })
  }

  // Scroll to next block

  $(".showcase-link").click(function () {
    $("html, body").animate({
      scrollTop: $($(this).attr("href")).offset().top + "px"
    }, {
      duration: 1000
    });
    return false;
  });

  // Fancybox

  $('[data-fancybox]').fancybox({
    infobar: false,
    toolbar: false,
    arrows: false,
    wheel: false,
    baseClass: "projects-fancybox",
    smallBtn: true,
    btnTpl: {
      smallBtn: '<div data-fancybox-close class="fancybox-close-small modal-close">Закрыть</div>'
    }
  });

  // Wow

  new WOW().init();

});