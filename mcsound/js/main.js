$(document).ready(function() {
  
  // Burger
  
 $(function(){
    $('.burger').click(function(){
      var content = $(this).next();
      var parent = $(this).parent();

      if( parent.hasClass('active') ) {
        content.fadeOut();
        parent.removeClass('active');
      } 
      else {
        content.fadeIn();
        parent.addClass('active');
      }
    });

    var over;

    $('.pattern-burger, .burger').mouseenter(function() {
      over = true;
    })
      .mouseleave(function() {
      over = false;
    });

    $(document).click(function(e) {
      if (!over) {
        $('.pattern-burger-wrap').removeClass('active');
        $('.pattern-burger').fadeOut();
      }
    });
 });
  
  // Scroll to next block
  
  $(function(){

    $(".showcase-img a").click(function() {

      $("html, body").animate({
        scrollTop: $($(this).attr("href")).offset().top + "px"
      }, {
        duration: 1000
      });

      return false;
    });

  }());
  
  // Main partners
  
  $('.main-partners-items').slick({
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    prevArrow: '<button type="button" class="slick-prev"></button>',
    nextArrow: '<button type="button" class="slick-next"></button>',
     responsive: [
    {
      breakpoint: 1336,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 1
      }
    },
     {
      breakpoint: 1200,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1
      }
    },
      {
      breakpoint: 767,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
      {
      breakpoint: 575,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
  
  // Main reviews
  
  $(function(){
    
    $('.review-items').slick({
      appendArrows: $('.arrows'),
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      prevArrow: '<button type="button" class="slick-prev"></button>',
      nextArrow: '<button type="button" class="slick-next"></button>'
    });
    
    $('.review-items').on('beforeChange', function(event, slick, currentSlide, nextSlide){
      $('.numbers__current').text(nextSlide + 1);
    });
    
    $('.numbers__total').text($('.review-item').length);
    
  });
  
  // Fancybox
  
  if($('[data-fancybox]').length) {
    $('[data-fancybox]').fancybox();
  }
  
});





