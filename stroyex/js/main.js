$(document).ready(function () {

  $('.burger').click(function () {
    $(this).children().toggleClass('active');
    $('.header__mobile').toggleClass('active');
    return false;
  });

  $('.showcase__inner').slick({
    dots: true
  });

  $('.search-result__carousel').slick({
    dots: true
  });

});