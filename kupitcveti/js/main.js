$(document).ready(function () {

  // Burger

  $('.burger').click(function () {
    $(this).children().toggleClass('active');
    $('.mobile-menu').toggleClass('active');
    return false;
  })

  // Datepicker

  $('.datepicker').datepicker({
    language: 'ru'
  });

  // Gallery

  $('.product-mini-item').click(function () {
    $('.product-mini-item').removeClass('active');
    $(this).addClass('active');

    var src = $(this).find('img').attr('src');
    $('.product-img').find('img').attr('src', src);
  })

  // Feedbacks slider

  $('.feedbacks-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    dots: true,
    arrows: false,
    responsive: [
      {
        breakpoint: 1336,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  })

  // Auth modal

  $('.auth-form button').on('click', function (e) {
    e.preventDefault()

    $('.auth-form').hide()
    $('.auth-confirm').show()
  })

  // Custom select

  $('.c-select-field').on('focus', function (e) {
    $('.c-select-list').removeClass('show')
    $(this).val('')
    $(this).parent().find('.c-select-list').addClass('show')
    console.log('test')
  })

  $('.c-select-list li').on('click', function (e) {
    $(this).closest('.c-select').find('.c-select-field').val($(this).html())
    $('.c-select-list').removeClass('show')
  })

  $(document).on('click', function (e) {
    if (!$(e.target).closest(".c-select").length) {
      $('.c-select-list').removeClass('show')
    }
    e.stopPropagation();
  });

  // Filter sublist

  $('.filter-more').on('click', function() {
    $('.filter-sublist').toggleClass('active')

    return false
  })

  $(window).on('scroll', function() {
    if($(this).scrollTop() > 700) {
      $('.fixed-header-wrap').addClass('active')
    } else {
      $('.fixed-header-wrap').removeClass('active')
    }
  })

});





