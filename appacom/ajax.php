<?php

error_reporting(E_ALL);
define ("_AJAX_LOAD", true);
define ('_INDEX', true);

try {
  $targetMail = 'info@mng-it.com';
  function send_mime_mail($name_from, // имя отправителя
                          $email_from, // email отправителя
                          $name_to, // имя получателя
                          $email_to, // email получателя
                          $data_charset, // кодировка переданных данных
                          $send_charset, // кодировка письма
                          $subject, // тема письма
                          $body, // текст письма
                          $html = FALSE, // письмо в виде html или обычного текста
                          $reply_to = FALSE
  )
  {
    $to = mime_header_encode($name_to, $data_charset, $send_charset)
        . ' <' . $email_to . '>';
    $subject = mime_header_encode($subject, $data_charset, $send_charset);
    $from = mime_header_encode($name_from, $data_charset, $send_charset)
        . ' <' . $email_from . '>';
    if ($data_charset != $send_charset) {
      $body = mb_convert_encoding($body, "KOI8-R", "UTF-8"); // @iconv($data_charset, $send_charset, $body);
    }
    $headers = "From: $from\r\n";
    $type = ($html) ? 'html' : 'plain';
    $headers .= "Content-type: text/$type; charset=$send_charset\r\n";
    $headers .= "Mime-Version: 1.0\r\n";
    if ($reply_to) {
      $headers .= "Reply-To: $reply_to";
    }
    $dat = mail($to, $subject, $body, $headers);
    return $dat;
  }

  function mime_header_encode($str, $data_charset, $send_charset)
  {
    if ($data_charset != $send_charset) {
      $str = mb_convert_encoding($str, "KOI8-R", "UTF-8");
    }
    return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
  }

  $r = new stdClass();
  $mess = array();

  if (empty ($_REQUEST['name'])) {
    $mess[] = "Введите Ваше имя";
  } else $name = $_REQUEST['name'];


  if (empty ($_REQUEST['phone'])) {
    $mess[] = "Введите номер телефона";
  } else $phone = $_REQUEST['phone'];

  if (empty ($_REQUEST['email'])) {
    $email = " ";
  } else $email = $_REQUEST['email'];


  if (empty ($_REQUEST['descr'])) {
    $descr = " ";
  } else $descr = $_REQUEST['descr'];


  if (empty ($mess) && !preg_match("/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/", $_REQUEST['phone'], $res)) {
    $mess[] = "Номер телефона указан неправильно";
  }

  if (empty ($mess)) {
    $title = "Уведомление с сайта";
    $mess = "Контактное лицо - " . $name
        . "\n\nТелефон - " . $phone
        . "\n\nEmail - " . $email
        . "\n\nСообщение - " . $descr;

    if (send_mime_mail(@$_REQUEST['name'], '', '', $targetMail, 'UTF-8', 'KOI8-R', $title, $mess)) {
      $r->success = true;
      $r->text = 'Ваш запрос отправлен.<br /> Мы свяжемся Вам в ближайшее время.';
    } else {
      $r->success = false;
      $r->err = 'Не удалось отправить сообщение, повторите попытку позже';
    }
  } else {
    $r->success = false;
    $tmp = '';
    foreach ($mess as $m) {
      $tmp .= $m . '<br />';
    }
    $r->err = $tmp;
  }
  echo json_encode($r);


} catch (E $e) {
  $e->write();
}



?>
