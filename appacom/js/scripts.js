$.ajaxSetup({
	dataType: "json",
	url: "/ajax.php",
	type: "POST"
});
$(document).ready(function() {
	$(".fancybox").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		height		: '70%',
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'fade',
		closeEffect	: 'none'
	});

	$('#callback form').submit(function(){
		var data = $(this).serialize();
		var p = $(this).parent();
		$.ajax({
			data: data,
			success: function(html){
				if (html.success==true) {
					$(p).find('form').remove();
					$(p).find('.err').html('');
					$(p).append(html.text);
				}else{
					$(p).find('.err').html(html.err);
				}
			}
		});
		return false
	})

	$('#order form').submit(function(){
		return false;
		var data = $(this).serialize();
		var p = $(this).parent();
		$.ajax({
			data: data,
			success: function(html){
				if (html.success==true) {
					$(p).find('form').remove();
					$(p).find('.err').html('');
					$(p).append(html.text);
				}else{
					$(p).find('.err').html(html.err);
				}
			}
		});

	})

	$('#feedback form').submit(function(){
		var data = $(this).serialize();
		var p = $(this).parent();
		$.ajax({
			data: data,
			success: function(html){
				if (html.success==true) {
					$(p).find('form').remove();
					$(p).find('.err').html('');
					$(p).append(html.text);
				}else{
					$(p).find('.err').html(html.err);
				}
			}
		});
		return false
	})
});

(function($) {
	// This is the connector function.
	// It connects one item from the navigation carousel to one item from the
	// stage carousel.
	// The default behaviour is, to connect items with the same index from both
	// carousels. This might _not_ work with circular carousels!
	var connector = function(itemNavigation, carouselStage) {
		return carouselStage.jcarousel('items').eq(itemNavigation.index());
	};

	$(function() {
		// Setup the carousels. Adjust the options for both carousels here.
		$('.carousel-navigation').jcarousel({auto: 2});
		// Setup controls for the navigation carousel
		$('.prev-navigation')
			.on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			})
			.on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			})
			.jcarouselControl({
				target: '-=1'
			});

		$('.next-navigation')
			.on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			})
			.on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			})
			.jcarouselControl({
				target: '+=1'
			});
	});
})(jQuery);


(function($) {
	// This is the connector function.
	// It connects one item from the navigation carousel to one item from the
	// stage carousel.
	// The default behaviour is, to connect items with the same index from both
	// carousels. This might _not_ work with circular carousels!
	var connector = function(itemNavigation, carouselStage) {
		return carouselStage.jcarousel('items').eq(itemNavigation.index());
	};

	$(function() {
		// Setup the carousels. Adjust the options for both carousels here.
		$('.carousel-stage').jcarousel({wrap:'circular'}).jcarouselAutoscroll({
			interval: 3000,
			target: '+=1',
			autostart: true
		});
		// Setup controls for the navigation carousel
		$('.prev-stage')
			.on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			})
			.on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			})
			.jcarouselControl({
				target: '-=1'
			});

		$('.next-stage')
			.on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			})
			.on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			})
			.jcarouselControl({
				target: '+=1'
			});
	});
})(jQuery);


(function($) {
	// This is the connector function.
	// It connects one item from the navigation carousel to one item from the
	// stage carousel.
	// The default behaviour is, to connect items with the same index from both
	// carousels. This might _not_ work with circular carousels!
	var connector = function(itemNavigation, carouselStage) {
		return carouselStage.jcarousel('items').eq(itemNavigation.index());
	};

	$(function() {
		// Setup the carousels. Adjust the options for both carousels here.
		$('.carousel-stage').jcarousel({wrap:'circular'}).jcarouselAutoscroll({
			interval: 3000,
			target: '+=1',
			autostart: true
		});
		// Setup controls for the navigation carousel
		$('.prev-stage')
			.on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			})
			.on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			})
			.jcarouselControl({
				target: '-=1'
			});

		$('.next-stage')
			.on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			})
			.on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			})
			.jcarouselControl({
				target: '+=1'
			});
	});
})(jQuery);