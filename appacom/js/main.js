$(function() {
    $('#calculator form').submit(function(){
        var data = $(this).serialize();
        var p = $(this).parent();
        $.ajax({
            data: data,
            dataType: "json",
            url: "/ajax.php",
            type: "POST",
            success: function(html){
                if (html.success==true) {
                    //$(p).find('form').remove();
                    $(p).find('input[type=text]').val('');
                    $(p).find('.err').html('');
                    $(p).find('.success').html(html.text);
                    $('#calculator form').hide()
                }else{
                    $(p).find('.err').html(html.err);
                }
            }
        });
        return false
    });

});

