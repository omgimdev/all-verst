$(document).ready(function() {
  
  // Burger
  
  $(function(){
  
    $('.burger').click(function(){
      $(this).children().toggleClass('active');
      $('.nav').toggleClass('active');
      return false;
    });

    $(document).on('click', function(e) {
      if (!$(e.target).closest(".nav").length) {
        $('.nav').removeClass('active');
        $('.burger a').removeClass('active');
      }
      e.stopPropagation();
    });

  }());
  
  // Main shares slider
  
  $('.shares-items').slick({
    infinite: true,
    dots: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    variableWidth: true,
    prevArrow: '<button type="button" class="slick-prev"><img src="images/left.png" alt=""></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="images/right.png" alt=""></button>',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });
  
  // Journal
  
  var mySwiper = new Swiper('.swiper-container', {
    slidesPerView: 3,
    spaceBetween: 30,
    loop: true,
    speed: 500,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    breakpoints: {
      1200: {
        spaceBetween: 24
      },
      991: {
        slidesPerView: 2,
        width: 650
      },
      767: {
        slidesPerView: 1,
        width: 300
      },
    }
  });
  
  // Main shares slider
  
  $('.reviews-items').slick({
    infinite: true,
    dots: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<button type="button" class="slick-prev"><img src="images/left.png" alt=""></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="images/right.png" alt=""></button>'
  });
  
  // License
  
  $('.license-items').slick({
    infinite: true,
    dots: true,
    slidesToShow: 3,
    slidesToScroll: 2,
    variableWidth: true,
    prevArrow: '<button type="button" class="slick-prev"><img src="images/leftB.png" alt=""></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="images/rightB.png" alt=""></button>'
  });
  
  // Expert
  
  var mySwiper = new Swiper('.expert-container', {
    slidesPerView: 3,
    spaceBetween: 60,
    loop: true,
    speed: 500,
    pagination: {
      el: '.expert-dots',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      1200: {
        spaceBetween: 46
      },
      991: {
        slidesPerView: 2
      },
      767: {
        slidesPerView: 1
      }
    }
  });
  
  // Programs
  
  $('.programs-head').click(function(){
    if($(this).hasClass('active')){
      $(this).removeClass('active');
      $(this).next().slideUp();
    }else{
      $('.programs-content').slideUp();
      $('.programs-head').removeClass('active');
      $(this).next().slideDown();
      $(this).addClass('active');
    }
  });
  
});





