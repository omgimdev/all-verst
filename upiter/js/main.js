var Page = {
        init: function() {
            this.currentProcessPartIndex = 0;
            this.beforeProcessPart = true;
            this.comparisonSliderInited = false;
            this.element = $('body');

            this.drawProcess();
            this.getObjects();

            var me = this;

            $('.js-process-tooltip').on('click', function(event) {
                var $el = $(event.currentTarget);
                event.preventDefault();

                var count = me.currentProcessPartIndex;
                me.currentProcessPartIndex = $el.closest('.js-step').prevAll('.js-step').length;
                me.initProcessPart();

                count = Math.max(1, count - me.currentProcessPartIndex);
                clearInterval(me.processPartAnimateInterval);
                setTimeout($.proxy(function() {
                    me.setProcessPartInterval();
                }), 600 * count);
            });

            $('.js-step#step-2 .js-process-tooltip').trigger('click');

        },

        '{window} resize': function() {
            this.getResizedValues();
            this.getScrolledValues();
            this.setValues();
            this.setMenuItem();
            this.setMenuPosition();
            this.setFixedContainerPosition();
        },

        '{window} scroll': function() {
            this.getScrolledValues();
            this.setMenuItem();
            this.setMenuPosition();
            this.setFixedContainerPosition();
        },

        /**
         * Получение значений и объектов
         */
        //получаем необходимые jQuery объекты
        getObjects: function() {
            // common
            this.$fullHeighs = this.element.find('.js-full-height');
            this.$menu = this.element.find('.js-menu');
            this.$menuItems = this.$menu.find('.js-menu-item');
            this.$fixedContainerList = this.element.find('.js-fixed-container');

            // main
            this.$mainPart = this.element.find('#main-part');

            // comparison
            this.$comparisonPart = this.element.find('#comparison-part');
            this.$comparisonSlider = this.$comparisonPart.find('.js-comparison-slideshow');

            // about

            //process
            this.$processPart = this.element.find('#process-part');

            // soft
            this.$softPart = this.element.find('#soft-part');
        },

        // получаем необходимые значения при старте страницы и ресайзе
        getResizedValues: function() {
            this.getScrolledValues();
            var $document = $(document);
            this.screenHeight = $(window).innerHeight();
            this.screenWidth = $(window).innerWidth();
            this.docHeight = $document.height();

            // comparison
            this.comparisonPartTop = this.$comparisonPart.position().top;
            this.setComparisonState();

            // process
            this.processPartTop = this.$processPart.position().top;
            this.getProcessPartIndex();
            this.setProcessState();

            this.menuItemsTop = [];
            this.menuItemsHeight = [];
            var self = this;
            this.$menuItems.each(function() {
                var $item = $(this);
                var $part = self.element.find($item.attr('href'));
                self.menuItemsTop.push($part.offset().top);
                self.menuItemsHeight.push($part.height());
            });
        },

        // получаем необходимые значения при старте страницы и скролле
        getScrolledValues: function() {
            var $document = $(document);
            //noinspection JSValidateTypes
            this.scrollTop = $document.scrollTop();
            //noinspection JSValidateTypes
            this.scrollLeft = $document.scrollLeft();

            // comparison
            this.setComparisonState();

            // process
            this.getProcessPartIndex();
            this.setProcessState();
        },

        // COMPARISON
        setComparisonState: function() {
            if(!this.comparisonSliderInited) {
                if (this.scrollTop >= this.comparisonPartTop) {
                    this.$comparisonSliderHidden.show();
                    this.$comparisonSlider.cycle('resume');
                    this.comparisonSliderInited = true;
                }
            }
        },

        //PROCESS
        setProcessState: function() {
            this.initProcessPart();
        },

        getProcessPartIndex: function() {
            if ((this.scrollTop + this.screenHeight) < this.processPartTop) {
                this.currentProcessPartIndex = 0;
                this.beforeProcessPart = true;
                clearTimeout(this.processPartAnimateInterval);
            }

            if (this.scrollTop > this.processPartTop && this.beforeProcessPart && this.scrollTop < this.processPartTop + 100) {
                 this.beforeProcessPart = false;
                 setTimeout(function(){
                     this.currentProcessPartIndex++;
                     if (this.currentProcessPartIndex > 4) {
                         this.currentProcessPartIndex = 0;
                     }
                     this.initProcessPart();
                 }.bind(this), 300);

                this.setProcessPartInterval();
            }
        },

        setProcessPartInterval: function() {
            var timeout = this.$processPart.data('timeout');
            timeout = timeout > 0 ? timeout : 3000;

            this.processPartAnimateInterval = setInterval(function(){
                this.currentProcessPartIndex++;

                if (this.currentProcessPartIndex > 4) {
                    this.currentProcessPartIndex = 4;
                    return;
                }

                this.initProcessPart();
            }.bind(this), timeout);
        },

        /**
         * Установка значений и состояний объектов
         */
        // устанавливаем размеры элментов
        setValues: function() {
            this.$fullHeighs.height(Math.max(this.screenHeight, this.options.siteMinHeight));
            this.getResizedValues();
        },

        // устанавливаем активный пункт меню
        setMenuItem: function() {
            var self = this;
            var $activeItem = this.$menuItems.last();

            if ((this.screenHeight + this.scrollTop + 50) < this.docHeight) {
                this.menuItemsTop.forEach(function(top, i, arr) {
                    var bottom = top + self.menuItemsHeight[i];
                    if(top <= self.scrollTop && bottom > self.scrollTop) {
                        $activeItem = self.$menuItems.eq(i);
                    }
                });
            }

            if (this.scrollTop <= 0) {
                $activeItem = this.$menuItems.first();
            }

            var $activePart = self.element.find($activeItem.attr('href'));

            if (!$activeItem.hasClass('active')) {
                this.$menuItems.removeClass('active');
                $activeItem.addClass('active');

                if($activePart.hasClass('main-part')) {
                    this.initMainPart();
                }

                if($activePart.hasClass('feedback-part')) {
                    this.initFeedbackPart();
                }
            }
        },

        // устанавливаем позицию меню
        setMenuPosition: function() {
            var calibrate = this.screenWidth < 1100 ? this.screenWidth - 1100 : 0;
            var left = this.options.menuLeft + calibrate;
            left = left < 10 ? 10 - this.scrollLeft  : left - this.scrollLeft;
            this.$menu.css({
                left: left
            });
        },

        setFixedContainerPosition: function() {
            this.$fixedContainerList.css({
                left: - this.scrollLeft
            });
        },

        /**
         * Инициализации
         */
        //scrolling animation library
        initSkrollr: function() {
            this.skr = skrollr.init({
                smoothScrolling: true,
                smoothScrollingDuration : 500,
                constants: {
                    hide_title: function(){
                        return this.comparisonEnd ? '-100p' : 0;
                    }.bind(this),
                    show_title: function(){
                        return this.comparisonEnd ? 0 : '199p';
                    }.bind(this),

                    hide_fight: function(){
                        return this.comparisonEnd ? '-100p' : 0;
                    }.bind(this),
                    show_fight: function(){
                        return this.comparisonEnd ? 0 : '199p';
                    }.bind(this),

                    hide_person: function(){
                        return this.comparisonEnd ? '-100p' : 0;
                    }.bind(this),
                    show_person: function(){
                        return this.comparisonEnd ? 0 : '199p';
                    }.bind(this),
                    start_admin_shadow: function(){
                        return this.comparisonEnd ? '-100p' : 0;
                    }.bind(this),
                    end_admin_shadow: function(){
                        return this.comparisonEnd ? '-100p' : '900p';
                    }.bind(this),
                    start_admin: function(){
                        return this.comparisonEnd ? '-100p' : '900p';
                    }.bind(this),
                    end_admin: function(){
                        return this.comparisonEnd ? 0 : '1000p';
                    }.bind(this)
                }
            });
        },

        // подёргивание кнопок через промежуток времени
        initShakeButton: function() {
            this.$buttons = this.element.find('.js-shake-me');
            this.$buttons.addClass('animated');
            this.shakeInterval = this.options.shakeInterval;
            this.setShakeInterval(this.shakeInterval);
        },

        setShakeInterval: function(time) {
            var interval = setInterval(this.proxy(function() {
                this.$buttons.addClass(this.options.shakeEffect);
                setTimeout(this.proxy(function() {
                    this.$buttons.removeClass(this.options.shakeEffect);
                }), 1000);
                this.shakeInterval = this.shakeInterval * 3;
                clearInterval(interval);
                this.setShakeInterval(this.shakeInterval);
            }), time);
        },

        /**
         *
         */
        initMainPart: function() {
            this.shakeInterval = this.options.shakeInterval;
            this.initShakeButton(this.shakeInterval);
        },

        initFeedbackPart: function() {
            this.shakeInterval = this.options.shakeInterval;
            this.initShakeButton(this.shakeInterval);
        },

        initProcessPart: function() {
            this.$stepList.removeClass('current');
            this.$stepList.removeClass('done');

            this.$stepList.eq(this.currentProcessPartIndex).addClass('current')
                .prevAll().addClass('done');

            this.$contentList.hide();
            this.$contentList.eq(this.currentProcessPartIndex).show();

            this.drawProcessPart(this.currentProcessPartIndex);
        },

        // отрисовываем изначальное состояние
        drawProcess: function() {
            this.centerX = 240;
            this.centerY = 240;
            this.radius = 205;
            this.$paper = Raphael("js-process-holder", 500, 500);
            this.$holder = this.element.find("#js-process-holder");
            this.$stepList = this.$holder.find('.js-step');
            this.$contentList = this.$holder.find('.js-circle-content');
            this.$paper.customAttributes.arc = function (xloc, yloc, value, total, R) {
                var alpha = 360 / total * value,
                    a = (90 - alpha) * Math.PI / 180,
                    x = xloc + R * Math.cos(a),
                    y = yloc - R * Math.sin(a),
                    path;
                if (total == value) {
                    path = [
                        ["M", xloc, yloc - R],
                        ["A", R, R, 0, 1, 1, xloc - 0.01, yloc - R]
                    ];
                } else {
                    path = [
                        ["M", xloc, yloc - R],
                        ["A", R, R, 0, +(alpha > 180), 1, x, y]
                    ];
                }
                return {
                    path: path
                };
            };

            var centerX = this.centerX;
            var centerY = this.centerY;
            var radius = this.radius;



            this.$arc = this.$paper.path().attr({
                "stroke": "#f15b46",
                "stroke-width": 11,
                arc: [centerX, centerY, 0, 5, radius]
            });
        },

        drawProcessPart: function(percent, callback) {
            var duration = Math.abs((this.lastPercent ? this.lastPercent : 0) - percent) * 600;
            this.lastPercent = percent;

            var centerX = this.centerX;
            var centerY = this.centerY;
            var radius = this.radius;

            this.$arc.stop().animate({
                arc: [centerX, centerY, percent, 5, radius]
            }, duration, callback ? callback : undefined);
        }
    };


$(document).ready(function() {
    Page.init()
});