$(document).ready(function () {

    //Burger menu
    $('.burger-wrap a').click(function () {
        $(this).toggleClass('active');
        $('.nav').toggleClass('active');
        return false;
    });

    //Carousel
    $('.showcase').slick({
        dots: true,
        fade: true
    });

    //Prices tabs
    $('.price-tab').click(function () {

        var current = $(this).index();

        $('.price-tab').removeClass('active');
        $(this).addClass('active');

        $('.price-content')
            .removeClass('active')
            .eq(current).addClass('active');
    });

    //Type tabs
    $('.type-tab').click(function () {

        var current = $(this).index();

        $('.type-tab').removeClass('active');
        $(this).addClass('active');

        $('.type-content')
            .removeClass('active')
            .eq(current).addClass('active');
    });

    //Cost tabs
    $('.cost-item--header').click(function () {

        var parent = $(this).parent(),
            content = $(this).next();

        if (parent.hasClass('active')) {
            content.slideToggle();
            parent.removeClass('active');
        }
        else {
            content.slideToggle();
            parent.addClass('active');
        }
    });

    // Fix header
    function header_fix() {
        var $elWrap = $('.header-wrap');

        if ($elWrap.hasClass('index-header')) {
            if ($(window).scrollTop() > $('.top-wrap').height()) {
                $elWrap.addClass('fixed');
                $('body').css({"padding-top": $elWrap.height()});
            }
            else {
                $('body').css({"padding-top": "0"});
                $elWrap.removeClass('fixed');
            }
        }
        else {
            $('body').css({"padding-top": $elWrap.height()});
        }
    }

    header_fix();

    $(window).on('scroll resize', function () {
        header_fix();
    });


    //Footer fixed
    function fixed() {
        var $elFooterWrap = $('.footer-wrap'),
            $elBotWrap = $('.bot-wrap'),
            bot = $elFooterWrap.outerHeight() + $elBotWrap.outerHeight();

        $('body').css({"padding-bottom": bot});
        $elFooterWrap.css({"bottom": $elBotWrap.outerHeight()});
    }

    fixed();

    $(window).resize(function () {
        fixed();
    })


});





