$(document).ready(function() {
  
  // Burger
  
  $('.burger-wrap').click(function(){
    $(this).children().toggleClass('active');
    $('.menu').toggleClass('active');
    return false;
  });
  
  // Main clients slider
  
  $('.main-clients-items').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    variableWidth: true,
    responsive: [
    {
      breakpoint: 1360,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1
      }
    },
      {
      breakpoint: 767,
      settings: {
        variableWidth: false,
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
      {
      breakpoint: 450,
      settings: {
        variableWidth: false,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
  
  // Officials slider
  
  $('.officials-items').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    variableWidth: true,
    responsive: [
    {
      breakpoint: 1360,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
      {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
      {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
  
  // Service slider
  
  $('.service-slider-items').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    variableWidth: true,
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        variableWidth: false,
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
      {
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
      {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
  
  // Serve slider
  
  $('.serve-slider-items').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
  
  // Clients slider
  
  $('.clients-slider-items').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 2,
    dots: true,
    variableWidth: true,
    responsive: [
    {
      breakpoint: 1360,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 2
      }
    },
      {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        variableWidth: false
      }
    },
      {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        arrows: false
      }
    }
  ]
  });
  
  // Working slider
  
  $('.working-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    responsive: [
    {
      breakpoint: 450,
      settings: {
        arrows: false
      }
    }
  ]
  });
  
  // More
  
  var moreHeight = $('.more-items').outerHeight();
  var moreItem = $('.more-item').outerHeight();
  
  $(window).resize(function(){
    var moreHeight = $('.more-items').outerHeight();
    var moreItem = $('.more-item').outerHeight();
  });
  
  $('.more-items').css({"height" : moreItem});
  
  $('.more-item_open').click(function(){
    
    var parent = $(this).parent();
    
    parent.toggleClass('active');
    
    if(parent.hasClass('active')) {
      $('.more-items').css({"height" : moreHeight});
    }
    else {
      $('.more-items').css({"height" : moreItem});
    }
    
    $(this).toggleClass('active');
    
  });
  
  // Fancybox
  
  if( $('[data-fancybox]').length()) {
    $('[data-fancybox]').fancybox();
  }
  
});





