$(document).ready(function() {
  
  // Burger
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.header-body').fadeToggle();
    return false;
  });
  
  $(window).resize(function(){
    
    if( $(window).width() > 991 ) {
      $('.header-body').removeAttr('style');
    }
    
    if( $(window).width() > 576) {
      $('.edition-content').removeAttr('style');
      $('.ratings-block').removeAttr('style');
      $('.edition-sort').removeAttr('style');
      $('.ratings-sort').removeAttr('style');
      $('.edition-item__head').removeClass('active');
      $('.ratings-head').removeClass('active');
    }
    
  });
  
  // Header section burger
  
  $('.header-section-burger').click(function(){
    $(this).toggleClass('active');
    $(this).children().toggleClass('active');
    $('.header-section-list').fadeToggle();
  });
  
  // Nav mobile
  
  $('.nav-list > a').click(function(e){
    if($(window).width() < 991){
      if($(this).next().length){
        e.preventDefault();
        $(this).toggleClass('active');
        $(this).next().slideToggle();
      }
    }
  });
  
  // City
  
  $('.city__link').click(function(e){
    e.preventDefault();
    $(this).parent().next().toggleClass('active');
  });
  
  // Search
  
  $('.search-btn').click(function(){
    $(".search input").toggleClass("active").focus;
    $(this).toggleClass("active");
    return false;
  });
  
  // Cabinet
  
  $('.cabinet-btn').click(function(){
    $(this).toggleClass('active');
    $(this).next().fadeToggle();
  });
  
  $('.cabinet-nav li').click(function(){
    if(!$(this).hasClass('active')) {
      $('.cabinet-nav li').removeClass('active');
      $(this).addClass('active');
      $('.cabinet-item').hide().removeClass('active');
      $('.cabinet-item').eq($(this).index()).fadeIn().addClass('active');
    }
  });
  
  // Quest
  
  $('.quest-slider').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    variableWidth: true,
    prevArrow: '<button type="button" class="slick-prev"><img src="images/left.png" alt=""></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="images/right.png" alt=""></button>',
  });
  
  // Edition
  
  $('.edition-item__head').click(function(){
    if($(window).width() < 575) {
      $(this).toggleClass('active');
      $(this).next().fadeToggle();
    }
  });
  
  // Ratings
  
  $('.ratings-head').click(function(){
    if($(window).width() < 575) {
      $(this).toggleClass('active');
      $(this).next().fadeToggle();
    }
  });
  
  // Blogs select
  
  $('.blogs-select__head').click(function(){
    if($(window).width() < 575) {
      $(this).toggleClass('active');
      $(this).next().fadeToggle();
    }
  });
  
  // Category favorites
  
  $('.category-btn__open').click(function(){
    $(this).fadeIn().removeClass('active');
    $(this).next().fadeOut().addClass('active');
    $(this).closest('.category-header').next().slideDown();
  });
  
  $('.category-btn__close').click(function(){
    $(this).fadeIn().removeClass('active');
    $(this).prev().fadeOut().addClass('active');
    $(this).closest('.category-header').next().slideUp();
  });
  
});





