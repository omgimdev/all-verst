$(document).ready(function() {
  
  // Feedbacks carousel
  
  $('.feedbacks-carousel').slick({
    slidesToScroll: 2,
    slidesToShow: 2,
    dots: true,
    variableWidth: true,
    appendArrows: $('.feedbacks-nav'),
    appendDots: $('.feedbacks-nav'),
    responsive: [
      {
        breakpoint: 1350,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
          variableWidth: false
        }
      }
    ]
  });
  
  // Scrool to next block
  
  function scroll_to() {
    $(".main__scroll").click(function() {
      var menu_height;
      $("html, body").animate({
        scrollTop: $($(this).attr("href")).offset().top + "px"
      }, {
        duration: 1000
      });
      $('.burger').removeClass('active');
      $('.nav').removeClass('nav_active');
      return false;
    });
  }

  scroll_to();
  
});





