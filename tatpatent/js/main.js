$(document).ready(function() {
  
  // Burger
  
  function burger(btn, el) {
    $(btn).click(function(){
      $(this).children().toggleClass('active');
      $(el).toggleClass('active');
      return false;
    });
  }

  burger('.burger-nav', '.nav-wrap');
  burger('.burger-top', '.top-links');

  // Scrollbar

  // var Scrollbar = window.Scrollbar;
  // Scrollbar.init(document.querySelector('.nav-container'));

  // Guidebook
  
  $('.guidebook-items').each(function() {
    $(this).slick({
      slidesToShow: 3,
      slidesToScroll: 2,
      dots: true,
      appendArrows: $(this).parent().find('.arrows'),
      appendDots: $(this).parent().find('.dots'),
      responsive: [
        {
          breakpoint: 1336,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
      ]
    })
  })

  // Event

  $('.event-items').each(function() {
    $(this).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      appendArrows: $(this).parent().find('.arrows'),
      appendDots: $(this).parent().find('.dots'),
    })
  })

  // Select2

  if($('.select2').length) {
    $('.select2').select2({
      width: '100%',
    });
  }

  // Card tabs

  $('.card-tabs li').click(function(){
    $('.card-tabs-item').hide().removeClass('active');
    $('.card-tabs-item').eq($(this).index()).fadeIn().addClass('active');
    $('.card-tabs li').removeClass('active');
    $(this).addClass('active');
  });

  // Check in

  $('.checkin-nav li').click(function(){
    $('.checkin-item').hide().removeClass('active');
    $('.checkin-item').eq($(this).index()).fadeIn().addClass('active');
    $('.checkin-nav li').removeClass('active');
    $(this).addClass('active');
  });
  
});





