$(document).ready(function() {


  //Burger menu
  $('.burger-wrap a').click(function(){
    $(this).toggleClass('active');
    $('.nav ul').toggleClass('active')
    return false;
  });
  
  // Main carousel
  
  $('.main-carousel').slick({
    fade: true,
    dots: true,
    arrows: false
  });
  
  // Feedback carousel
  
  $('.feedbacks-carousel').slick({
    speed: 300
  });
  
  // Fancybox
  
  $('.fancybox-city').fancybox({
    padding: 0,
    closeBtn: false
  });
  
  $('.city__close').click(function() {
    $.fancybox.close();
  });
  
  $('[data-city]').click(function() {
    
    var text = $(this).text();
    var value = $(this).attr('data-city');
    
    $('.callback__main').text(text);
    $('.footer-callback__main').text(text);
    $('[data-city-val]').hide();
    $('[data-city-val]').each(function() {
      if( $(this).attr('data-city-val').toString() == value.toString() ) {
        $(this).show();
      }
    })
    
    $.fancybox.close();
    return false;
    
  });
  
  $('.fancybox-register').fancybox({
    padding: 0
  });
  
  $('.modal__tab_entrance').click(function() {
    
    if(! $(this).hasClass('active') ) {
      $.fancybox.open( 
        [{ href : '#entrance' }], 
        {
          padding: 0
        }
      )
    }
    
  });
  
  $('.modal__tab_reg').click(function() {
    
    if(! $(this).hasClass('active') ) {
      $.fancybox.open( 
        [{ href : '#reg' }], 
        {
          padding: 0
        }
      )
    }
    
  });
  
  $('.fancybox-gallery').fancybox({
    padding: 0
  })
  
  // Footer stick
  
  function footer_stick() {
    $('body').css('padding-bottom', $('.footer-stick').outerHeight());
  }
  
  footer_stick();
  
  $(window).resize(function() {
    footer_stick();
  })
  
  // Ask drop down
  
  $('.ask-item-header').click(function() {
    
    $(this).parent().toggleClass('active');
    $(this).next().slideToggle(300);
    
  });
  
  //Sidebar
  
  $('.catalog-item__head').click(function() {
    
    $(this).next().slideToggle(300);
    $(this).parent().toggleClass('active');
    
  });
  
  if( $( ".catalog-cost-slider" ).length ) {

    $( ".catalog-cost-slider" ).slider({
      range: true,
      min: 0,
      max: 50000,
      values: [ 100, 21500 ],
      slide: function( event, ui ) {
        $( ".catalog__cost1" ).val( ui.values[ 0 ] );
        $( ".catalog__cost2" ).val( ui.values[ 1 ] );
      }
    });

    $( ".catalog__input input" ).change(function() {
      $( ".catalog-cost-slider" ).slider( "option", "values", [
        $( ".catalog__cost1" ).val(),
        $( ".catalog__cost2" ).val()
      ] );
    });

  }
  
  if( $( ".catalog-height-slider" ).length ) {

    $( ".catalog-height-slider" ).slider({
      range: true,
      min: 0,
      max: 1000,
      values: [ 30, 500 ],
      slide: function( event, ui ) {
        $( ".catalog__height1" ).val( ui.values[ 0 ] );
        $( ".catalog__height2" ).val( ui.values[ 1 ] );
      }
    });

    $( ".catalog__input input" ).change(function() {
      $( ".catalog-height-slider" ).slider( "option", "values", [
        $( ".catalog__height1" ).val(),
        $( ".catalog__height2" ).val()
      ] );
    });

  }
  
  // Bucket amount
  
  $('.catalog-elem__plus').click(function() {
    
    var bucket = $(this).parent().find('.catalog-elem__val');
    bucket.val( +bucket.val() + 1 );
    
  });
  
  $('.catalog-elem__minus').click(function() {
    
    var bucket = $(this).parent().find('.catalog-elem__val');
    if( bucket.val() != 1 ) {
      bucket.val( +bucket.val() - 1 );
    }
    
  });
  
  //Mask input
  
  if ( $('.input-age').length ) {
    $('.input-age').mask("00/00/0000", {placeholder: "../../...."});
  }
  if ( $('.input-age').length ) {
    $('.input-tel').mask("+7(000) 000-00-00", {placeholder: "+7"}); 
  }
  
   
  
});





