$(document).ready(function() {
  
  // Burger
  
  $(function(){
  
    $('.burger-wrap').click(function(){
      $(this).children().toggleClass('active');
      $('.nav').toggleClass('active');
      return false;
    });

    $(document).on('click', function(e) {
      if (!$(e.target).closest(".nav").length) {
        $('.nav').removeClass('active');
        $('.burger-wrap a').removeClass('active');
      }
      e.stopPropagation();
    });

  }());
  
  // Header select
  
  $('.header-select__name').click(function(){
    $(this).children().toggleClass('active');
    $('.header-select-list').toggleClass('active');
    
    if( $('.header-select-list').hasClass('active') ){
      $('.header-select__name').addClass('active');
    }
    else {
      $('.header-select__name').removeClass('active');
    }
    
    return false;
  });
  
  $(document).on('click', function(e) {
    if (!$(e.target).closest(".header-select-list").length) {
      $('.header-select-list').removeClass('active');
      $('.header-select__name').removeClass('active');
    }
    e.stopPropagation();
  });
  
  // Select header
  
  $(".header-select select").select2();
  
  // Main-services tab
  
  $('.tab-nav li').click(function(){
    
    var current = $(this).index();
    
    $('.tab-content').hide();
    $('.tab-content').eq(current).fadeIn();
    $('.tab-nav li').removeClass('active');
    $(this).addClass('active');
  });
  
  // Offers nav
  
  $('.offers-nav li').click(function(){
    
    var current = $(this).index();

    $('.offers-content').hide();
    $('.offers-content').eq(current).fadeIn();
    $('.offers-nav li').removeClass('active');
    $(this).addClass('active');
    
  });
  
  // Main slider items
  
  $('.main-slider-items').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    fade: true
  });
  
  // Showcase slider
  
  $('.showcase-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true
  });
  
  // Callback tab
  
  $('.callback-nav li').click(function(){
    
    var current = $(this).index();

    $('.callback-content').hide();
    $('.callback-content').eq(current).fadeIn();
    $('.callback-nav li').removeClass('active');
    $(this).addClass('active');
    
  });
  
  // Portfolio slider
  
  $('.portfolio-slider-wrap').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
    responsive: [
    {
      breakpoint: 767,
      settings: {
        arrows: false
      }
    }
  ]
  });
  
  // Concise
  
  $('.data-header').click(function(){
    
    var content = $(this).next();
    
    $(this).toggleClass('active');
    content.slideToggle();
    $('.data-img').toggleClass('active');

//    $(this).toggleClass('active');
//    content.slideToggle();
  });
  
  // Option
  
  $('.option-elem-head').click(function(){
    var parent = $(this).parent();
    var content = $(this).next();
    
    if(parent.hasClass('active')) {
      content.slideUp();
      parent.removeClass('active');
    }
    else {
      $('.option-elem-content').slideUp();
      $('.option-elem').removeClass('active');
      content.slideDown();
      parent.addClass('active');
    }
  });
  
  
});





