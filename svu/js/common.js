$(document).ready(function() {
  
  //Burger

  $(function(){
  
    $('.burger-wrap').click(function(){
      $(this).children().toggleClass('active');
      $('.nav ul').toggleClass('active');
      return false;
    });

    $(document).on('click', function(e) {
      if (!$(e.target).closest(".nav").length) {
        $('.nav ul').removeClass('active');
        $('.burger-wrap a').removeClass('active');
      }
      e.stopPropagation();
    });

  }());
  
  // Team slider
  
  $('.team-items').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
    responsive: [
    {
      breakpoint: 1350,
      settings: {
        slidesToShow: 3
      }
    },
      {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2
      }
    },
      {
      breakpoint: 767,
      settings: {
        slidesToShow: 1
      }
    }
  ]
  });
  
  // Scroll to next block
  
  function scroll_to() {
    $(".arrow-down").click(function() {
      
      $("html, body").animate({
        scrollTop: $($(this).attr("href")).offset().top + "px"
      }, {
        duration: 1000
      });
      
      return false;
    });
  }
  
  scroll_to();
  
  // Frame slider
  
  $('.clients-items').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    variableWidth: true,
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-long-arrow-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-long-arrow-right"></i></button>',
    responsive: [
    {
      breakpoint: 1350,
      settings: {
        slidesToShow: 4
      }
    },
      {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3
      }
    },
      {
      breakpoint: 767,
      settings: {
        slidesToShow: 2
      }
    },
      {
      breakpoint: 575,
      settings: {
        slidesToShow: 1
      }
    }
  ]
  });
  
  
});





