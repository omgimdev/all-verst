$(document).ready(function() {
  
  // Burger
  
  $(function(){
  
    $('.burger').click(function(){
      $(this).children().toggleClass('active');
      $('.nav-wrap').toggleClass('active');
      return false;
    });

    $(document).on('click', function(e) {
      if (!$(e.target).closest(".nav-wrap").length) {
        $('.nav-wrap').removeClass('active');
        $('.burger a').removeClass('active');
      }
      e.stopPropagation();
    });

  }());
  
  // Showcase
  
  $('.showcase-wrap, .offer-items').slick({
    infinite: true,
    dots: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-circle-left"></i></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-circle-right"></i></button>'
  });
  
  $('.slick-dots li button').each(function(){
    $(this).html('');
  });
  
});





