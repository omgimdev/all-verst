$(document).ready(function () {

  // Burger

  $('.burger').click(function () {
    $(this).children().toggleClass('active');
    $('.mobile-menu').toggleClass('active');
    return false;
  })

  // Carousels

  $('.slider-items').owlCarousel({
    items: 1,
    dots: false,
    nav: true,
    margin: 10,
    responsive: {
      767: {
        items: 2,
        margin: 60,
      },
      991: {
        items: 3,
        margin: 60
      }
    }
  })

  $('.feedback-slider').owlCarousel({
    items: 1,
    dots: false,
    nav: true,
    margin: 10
  })

  $('.blog-slider').owlCarousel({
    items: 1,
    dots: false,
    nav: true,
    margin: 10
  })

});





