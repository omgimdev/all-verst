(function(JQuery){
  
  // Change geo data
  function change_geo_date() {
    var index_city = 0;
    $('.menu-contacts-geo-cities--list').each(function() {
      if($(this).hasClass('menu-contacts-geo-cities--list_active')) {
        return false; 
      }
      index_city++;
    });
    $('.geo-tel').css({"display": "none"});
    $('.geo-tel').eq(index_city).css({"display": "inline-block"});
    $('.geo-address').css({"display": "none"});
    $('.geo-address').eq(index_city).css({"display": "inline-block"});
  }
  
  change_geo_date();
  
  // Carousel in index page
  
  $('.service-carousel').slick({
    arrows: false,
    fade: true,
    draggable: false,
    infinite: true,
  });
  
  var count = $('.service-item').size();
  $('.service-count--all').text(count);
  
  function current_items() {
    var current = $('.service-carousel').slick('slickCurrentSlide');
    $('.service-count--current').text(current+1);
  }
  
  $('.service-count--prev').click(function() {
    $('.service-carousel').slick('slickPrev');
    current_items();
    go_current_tab()
  });
  
  $('.service-count--next').click(function() {
    $('.service-carousel').slick('slickNext');
    current_items();
    go_current_tab()
  });
  
  $('.service-nav-item').click(function() {
    $('.service-nav-item').removeClass('service-nav-item_active');
    $(this).addClass('service-nav-item_active');
    var current_tab = $('.service-nav-item_active').index();
    $('.service-carousel').slick('slickGoTo', current_tab.toString())
    current_items();
  });
  
  function go_current_tab() {
    $('.service-nav-item').removeClass('service-nav-item_active');
    $('.service-nav-item').eq($('.service-carousel').slick('slickCurrentSlide')).addClass('service-nav-item_active');
  }
    
  var carousel_interval = setInterval(function() {
    $('.service-carousel').slick('slickNext');
    go_current_tab();
    current_items();
  },5000);
   
  
  function mobile_serve() {
    if($(window).width() > 768) {
      $('.service-nav-item').click(function(e) {
         if($(window).width() > 768) {
           return false;
         }
      });
    }
    else {
      $('.service-nav-item').click(function(e) {
         return true;
      });
    }
  }
  
  mobile_serve();
  
  $(window).resize(function() {
     mobile_serve();
  });
  
  // Border animation
  
  $('.border-animate').hover(function() {
    $(this).find('.line1').stop(true).delay().animate({width : "100%"}, 300);
    $(this).find('.line2').stop(true).delay(280).animate({height : "100%"}, 300);
    $(this).find('.line3').stop(true).delay(580).animate({width : "100%"}, 300);
    $(this).find('.line4').stop(true).delay(880).animate({height : "100%"}, 300);
  },
                             function() {
    $(this).find('.line4').stop(true).delay().animate({height : "0"}, 300);
    $(this).find('.line3').stop(true).delay(280).animate({width : "0"}, 300);
    $(this).find('.line2').stop(true).delay(580).animate({height : "0"}, 300);
    $(this).find('.line1').stop(true).delay(880).animate({width : "0"}, 300);
    
  });
  
  $('.main-cases-item-hidden').hover(function() {
    $(this).find('.line1').stop(true).delay().animate({width : "100%"}, 300);
    $(this).find('.line2').stop(true).delay(280).animate({height : "100%"}, 300);
    $(this).find('.line3').stop(true).delay(580).animate({width : "100%"}, 300);
    $(this).find('.line4').stop(true).delay(880).animate({height : "100%"}, 300);
  },
                                    function() {
    $(this).find('.line4').css({'height' : "0"});
    $(this).find('.line3').css({'width' : "0"});
    $(this).find('.line2').css({'height' : "0"});
    $(this).find('.line1').css({'width' : "0"});
    
  })
  
  // Scrool to second window
  
  $('.scroll').click(function() {
    $("html, body").animate({
			scrollTop: $('.about-wrap').offset().top + 50 + "px"
		}, {
			duration: 1000
		});
  })
  
  // Animate menu 
  
  var changer = true;
  

  $('.menu-wrap').hover(function () {
    if ($(window).width() > 767) {
      if (changer) {
        $(this).addClass('menu-wrap_active');
      }
      changer = false;
    }
  });
  $('.menu-logo--hidden-img').click(function() {
    if($(window).width() > 767) {
      $('.menu-wrap').removeClass('menu-wrap_active');
      setTimeout(function() {
        changer = true;
      }, 1000);
    }
  });
  
  $('.menu-logo').click(function() {
    if($(window).width() < 768) {
      $('.menu-nav').fadeToggle();
      $('.menu-contacts').fadeToggle();
      $('.menu-logo--main-img').fadeToggle();
      $('.menu-logo--hidden-img').fadeToggle();
      $('.menu-wrap').toggleClass('menu-wrap_bg');
    }
  })
  
  // Geo list
  
  $('.menu-contacts-geo-cities--list').on('click', function(e) {
    e.preventDefault();
  });
  
  $('.menu-contacts-geo--current').click(function () {
    
    if($(window).width() > 767) {
      var current_width = $(this).width();
    
      $('.menu-contacts-geo-cities').css({"opacity" : "1", "z-index" : "5", "margin-top" : "-55px"});
      $(this).css({"opacity" : "0"});
      $('.menu-contacts-geo--text').css({"margin-left": current_width + 11});
    }
    else {
      $('.menu-contacts-geo-cities').show();
    }
    
  });
  
  $('.menu-contacts-geo-cities--list').click(function() {
    if($(window).width() > 767) {
      $('.menu-contacts-geo-cities').css({"opacity" : "0", "z-index" : "-1", "margin-top" : "50px"});
      $('.menu-contacts-geo--current').text($(this).children().html()).css({"opacity" : "1"});
      $('.menu-contacts-geo-cities--list').removeClass('menu-contacts-geo-cities--list_active');
      $('.menu-contacts-geo--text').css({"margin-left": "0"});
      $(this).addClass('menu-contacts-geo-cities--list_active');
    }
    else {
      $('.menu-contacts-geo--current').text($(this).children().html())
      $('.menu-contacts-geo-cities').hide();
      $('.menu-contacts-geo-cities--list').removeClass('menu-contacts-geo-cities--list_active');
      $(this).addClass('menu-contacts-geo-cities--list_active');
    };
    change_geo_date();
  })
  
  // carousel
  
  $('.partners-carousel').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    arrow: true,
    touchMove: false,
    responsive: [
    {
      breakpoint: 1365,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
  
  // Placeholder Brief inputs on mobile devices
  
  function brief_placeholer() {
    if($(window).width() < 768) {
      $('.brief-elem--input').attr("placeholder", "");
    }
  };
  
  brief_placeholer();
  
  $(window).resize(function() {
     brief_placeholer();
  })
  
  

  
})(this);