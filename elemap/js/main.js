$(document).ready(function() {
  
  // Burger
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.nav').toggleClass('active');
    return false;
  });
  
  // Serve
  
  $('.serve-tabs li').click(function(){
    $('.serve-item').hide().removeClass('active');
    $('.serve-item').eq($(this).index()).fadeIn().addClass('active');
    $('.serve-tabs li').removeClass('active');
    $(this).addClass('active');
  });
  
});





