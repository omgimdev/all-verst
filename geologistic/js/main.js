$(document).ready(function() {
  
  // Burger
  
  $('.burger').click(function(){
    $(this).children().toggleClass('active');
    $('.nav').toggleClass('active');
    $('.header-content').toggleClass('active');
    return false;
  })

  // Partners slider

  $('.partners-items').slick({
    dots: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    speed: 100,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      }
    ]
  });

  // Contacts tabs

  var tab_index;

  $('.contacts-location-tab').click(function() {
    tab_index = $(this).index();
    $('.contacts-location-tab').removeClass('active');
    $(this).addClass('active');
    $('.contacts-location-address').removeClass('active');
    $('.contacts-location-address').eq(tab_index).addClass('active');
    $('.contacts-location-map').removeClass('active');
    $('.contacts-location-map').eq(tab_index).addClass('active');
  })

  // Fixed banner

  var form,
      parent,
      pos_left,
      form_width

  parent = $('.with-fixed').parent().css({
    position: 'relative'
  })
  
  $(window).scroll(function() {
    $('.with-fixed').each(function() {
      form = $(this);
      parent = form.parent();
      pos_left = parent.offset().left;
      pos_top = parent.offset().top - 10;
      form_width = parent.outerWidth()
      if ($(window).scrollTop() > pos_top) {
        form.addClass('fixed').css({
          left: pos_left,
          width: form_width
        });
      } else {
        form.removeClass('fixed').css({
          left: '0px'
        })
      }
      if ($(window).scrollTop() > pos_top + parent.outerHeight() - form.outerHeight() - 85) {
        form.addClass('stopped')
      } else {
        form.removeClass('stopped')
      }
    })
  });

  $(window).resize(function() {
    $('.with-fixed').each(function() {
      form = $(this);
      parent = form.parent();
      pos_left = parent.offset().left;
      form_width = parent.outerWidth();
      if (form.hasClass('fixed')) {
        form.css({
          left: pos_left,
          width: form_width
        });
      }
    });
  });

  // Input mask
  // https://igorescobar.github.io/jQuery-Mask-Plugin/docs.html

  $('.input-mask').mask("+7 000-00-00", {placeholder: "Ваш телефон"});

});





