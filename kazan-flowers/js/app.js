$.ajaxSetup({
    dataType: "json",
    url: "/forms.php",
    type: "POST"
});

$(document).ready(function(){

    $('.js-social-tabs a').click(function(){
        var id = $(this).data('tab');

        $('.js-social-tabs a').removeClass('active');
        $(this).addClass('active');

        $('.js-social-tabs-content div').addClass('hidden').filter('[data-id='+id+']').removeClass('hidden');
        return false
    });

    cuSel({
            changedEl: ".js-select",
            visRows: 5,
            scrollArrows: true
        });

});